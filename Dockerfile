FROM node:alpine

WORKDIR /home/node/app
COPY . /home/node/app

RUN [ "chmod", "+x", "install-start.sh" ]
ENTRYPOINT [ "./install-start.sh" ]

EXPOSE 8081
