/**
 * @swagger
 * definitions:
 *   Post:
 *     type: object
 *     required:
 *       - title
 *       - content
 *     properties:
 *       id:
 *         type: number
 *       title:
 *         type: string
 *       content:
 *         type: string
 */
export default class Post {
  constructor(id, title, content) {
    this.id = id;
    this.title = title;
    this.content = content;
  }
}
