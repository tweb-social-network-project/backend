/**
 * @swagger
 * definitions:
 *   Tag:
 *     type: object
 *     required:
 *       - name
 *     properties:
 *       id:
 *         type: number
 *       name:
 *         type: string
 */
export default class Tag {
  constructor(id, name) {
    this.id = id;
    this.name = name;
  }
}
