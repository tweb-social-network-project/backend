/**
 * @swagger
 * definitions:
 *   User:
 *     type: object
 *     required:
 *       - firstName
 *       - lastName
 *       - email
 *     properties:
 *       id:
 *         type: number
 *       firstName:
 *         type: string
 *       lastName:
 *         type: string
 *       email:
 *         type: string
 *       passwordHash:
 *         type: string
 */
export default class User {
  constructor(id, firstName, lastName, email, passwordHash) {
    this.id = id;
    this.firstName = firstName;
    this.lastName = lastName;
    this.email = email;
    this.passwordHash = passwordHash;
  }
}
