'use strict'

const chai = require('chai')
const chaiAsPromised = require('chai-as-promised')

chai.use(chaiAsPromised)
chai.should()

const TestHelper = require('./helper.js')
const PostManager = require('../repositories/post.js')
const UserRepository = require('../repositories/user.js')
const TagManager = require('../repositories/tag.js')

// configure database connection
const neo4j = require('neo4j-driver').v1
const graphenedbURL = 'bolt://localhost:7687'
const graphenedbUser = 'neo4j'
const graphenedbPass = '12345'
const driver = neo4j.driver(
  graphenedbURL,
  neo4j.auth.basic(graphenedbUser, graphenedbPass),
  { disableLosslessIntegers: true }
)

const helper = new TestHelper(driver)
const manager = new PostManager(driver)
const userRepo = new UserRepository(driver)
const tagManager = new TagManager(driver)

describe('Post manager', () => {

    describe('Create post', () => {
        
        afterEach((done) => {
            helper.cleanDatabase()
                .then(() => {
                    done()
                })
        })

        it('should fail if content property is missing', () => {
            manager.create({ other: 'some value' }).should.be.rejected
        })

        it('should fail if author id is missing', () => {
            manager.create({ content: 'some content' }).should.be.rejected
        })

        it('should fail if author does not exist', () => {
            manager.create({ content: 'some content' }, 'unknown').should.be.rejected
        })

        it('should succeed if author exist', (done) => {
            userRepo.create({ email: 'bob@marley.com', passwordHash: '12345' })
                .then((user) => {
                    return manager.create({ content: 'some content' }, user.id)
                })
                .then((post) => {
                    post.should.have.property('id')
                    post.should.have.property('created')
                    done()
                })
        })
        
        it('should create a reference between the post and a tag included in the post', (done) => {
            userRepo.create({ email: 'bob@marley.com', passwordHash: '12345' })
                .then((user) => {
                    manager.create({ content: 'some #awesome content' }, user.id)
                        .then((post) => {
                            tagManager.getReferencingNodes('#awesome')
                                .then((nodes) => {
                                    nodes.should.not.be.empty
                                    nodes[0].id.should.equal(post.id)
                                    done()
                                })
                        })
                })
        })
        
        it('should create multiple references between the post and the tags included in the post', (done) => {
            userRepo.create({ email: 'bob@marley.com', passwordHash: '12345' })
                .then((user) => {
                    manager.create({ content: 'some #awesome content about #gardening' }, user.id)
                        .then((post) => {
                            tagManager.getReferencingNodes('#awesome')
                                .then((nodes) => {
                                    nodes.should.not.be.empty
                                    nodes[0].id.should.equal(post.id)
                                    tagManager.getReferencingNodes('#gardening')
                                        .then((nodes) => {
                                            nodes.should.not.be.empty
                                            nodes[0].id.should.equal(post.id)
                                            done()
                                        })
                                })
                        })
                })
        })

        it('should fail if replyTo post does not exist', () => {
            userRepo.create({ email: 'bob@marley.com', passwordHash: '12345' })
                .then((user) => {
                    manager.create({ content: 'some content' }, user.id, 'unknown').should.be.rejected
                })
        })

        it('should link the post to the post it replies to', (done) => {
            userRepo.create({ email: 'bob@marley.com', passwordHash: '12345' })
                .then((user) => {
                    manager.create({ content: 'some content' }, user.id)
                        .then((post1) => {
                            manager.create({ content: 'some answer to some content' }, user.id, post1.id)
                                .then(() => {
                                    manager.getAnswers(post1.id)
                                        .then((answers) => {
                                            answers.should.be.instanceof(Array)
                                            answers.should.not.be.empty
                                            done()
                                        })
                                })
                        })
                })
        })
    })

    describe('Get all posts', () => {

        afterEach((done) => {
            helper.cleanDatabase()
                .then(() => {
                    done()
                })
        })

        it('should return an empty array if no post exist', (done) => {
            manager.getAll()
                .then((posts) => {
                    posts.should.be.instanceof(Array)
                    posts.should.be.empty
                    done()
                })
        })

        it('should return a non-empty array if posts exist', (done) => {
            userRepo.create({ email: 'bob@marley.com', passwordHash: '12345' })
                .then((user) => {
                    return manager.create({ content: 'some content' }, user.id)
                })
                .then(() => {
                    return manager.getAll()
                })
                .then((posts) => {
                    posts.should.be.instanceof(Array)
                    posts.should.not.be.empty
                    posts[0].should.have.property('post')
                    posts[0].should.have.property('author')
                    posts[0].author.should.not.have.property('passwordHash')
                    done()
                })
        })
    })

    describe('Get user posts', () => {

        afterEach((done) => {
            helper.cleanDatabase()
                .then(() => {
                    done()
                })
        })

        it('should return an empty array if user does not exist', (done) => {
            manager.getUserPosts('unknown')
                .then((posts) => {
                    posts.should.be.instanceof(Array)
                    posts.should.be.empty
                    done()
                })
        })

        it('should return an empty array if user exist but has not posted yet', (done) => {
            userRepo.create({ email: 'bob@marley.com', passwordHash: '12345' })
                .then((user) => {
                    return manager.getUserPosts(user.id)
                })
                .then((posts) => {
                    posts.should.be.instanceof(Array)
                    posts.should.be.empty
                    done()
                })
        })

        it('should return posts if they exist', (done) => {
            userRepo.create({ email: 'bob@marley.com', passwordHash: '12345' })
                .then((user) => {
                    manager.create({ content: 'some content' }, user.id)
                        .then(() => {
                            manager.getUserPosts(user.id)
                                .then((posts) => {
                                    posts.should.be.instanceof(Array)
                                    posts.should.have.length(1)
                                    done()
                                })
                        })
                })
        })

        it('should return posts ordered by created date (descending)', done => {
            userRepo.create({ email: 'bob@marley.com', passwordHash: '12345' })
                .then((user) => {
                    manager.create({ content: 'some old content' }, user.id)
                        .then(() => {
                            return manager.create({ content: 'some new content' }, user.id)
                        })
                        .then(() => {
                            return manager.getUserPosts(user.id)
                        })
                        .then((posts) => {
                            posts.should.be.instanceof(Array)
                            posts.should.have.length(2)
                            posts[0].content.should.equal('some new content')
                            posts[1].content.should.equal('some old content')
                            done()
                        })
                    })
        })
    })

    describe('Get post answers', () => {

        afterEach((done) => {
            helper.cleanDatabase()
                .then(() => {
                    done()
                })
        })

        it('should return an empty array if post does not exist', (done) => {
            manager.getAnswers('unknown')
                .then((answers) => {
                    answers.should.be.instanceof(Array)
                    answers.should.be.empty
                    done()
                })
        })

        it('should return an empty array if post exist but has no answer yet', (done) => {
            userRepo.create({ email: 'bob@marley.com', passwordHash: '12345' })
                .then((user) => {
                    return manager.create({ content: 'some content' }, user.id)
                })
                .then((post) => {
                    return manager.getAnswers(post.id)
                })
                .then((answers) => {
                    answers.should.be.instanceof(Array)
                    answers.should.be.empty
                    done()
                })
        })

        it('should return a non-empty array if post exist and has answers', (done) => {
            userRepo.create({ email: 'bob@marley.com', passwordHash: '12345' })
                .then((user) => {
                    manager.create({ content: 'some content' }, user.id)
                        .then((post1) => {
                            manager.create({ content: 'some answer' }, user.id, post1.id)
                                .then((post2) => {
                                    manager.getAnswers(post1.id)
                                        .then((answers) => {
                                            answers.should.be.instanceof(Array)
                                            answers.should.not.be.empty
                                            answers[0].answer.id.should.equal(post2.id)
                                            answers[0].author.id.should.equal(user.id)
                                            done()
                                        })
                                })
                        })
                })
        })
    })
    
    describe('Delete post', () => {
        
        afterEach((done) => {
            helper.cleanDatabase()
                .then(() => {
                    done()
                })
        })

        it('should fail if post id is missing', () => {
            manager.delete().should.be.rejected
        })

        it('should fail if post does not exist', (done) => {
            manager.delete('unknown').should.be.rejected
            done()
        })

        it('should succeed if post exist', (done) => {
            userRepo.create({ email: 'bob@marley.com', passwordHash: '12345' })
                .then((user) => {
                    return manager.create({ content: 'some content' }, user.id)
                })
                .then((post) => {
                    return manager.delete(post.id)
                })
                .then(() => {
                    done()
                })
        })
    })
    
    describe('Search posts', () => {
        
        afterEach((done) => {
            helper.cleanDatabase()
                .then(() => {
                    done()
                })
        })

        it('should fail if word is missing', () => {
            manager.findByWord().should.be.rejected
        })

        it('should return an empty array if no posts exist', (done) => {
            userRepo.create({ email: 'bob@marley.com', passwordHash: '12345' })
                .then(() => {
                    return manager.findByWord('other')
                })
                .then((posts) => {
                    posts.should.be.instanceof(Array)
                    posts.should.be.empty
                    done()
                })
        })

        it('should return an empty array if posts exist but not with searched word', (done) => {
            userRepo.create({ email: 'bob@marley.com', passwordHash: '12345' })
                .then((user) => {
                    return manager.create({ content: 'some content' }, user.id)
                })
                .then(() => {
                    return manager.findByWord('other')
                })
                .then((posts) => {
                    posts.should.be.instanceof(Array)
                    posts.should.be.empty
                    done()
                })
        })

        it('should return a non-empty array if posts with word exist', (done) => {
            userRepo.create({ email: 'bob@marley.com', passwordHash: '12345' })
                .then((user) => {
                    return manager.create({ content: 'some content' }, user.id)
                })
                .then(() => {
                    return manager.findByWord('content')
                })
                .then((posts) => {
                    posts.should.be.instanceof(Array)
                    posts.should.not.be.empty
                    posts[0].should.have.property('post')
                    posts[0].should.have.property('author')
                    done()
                })
        })
    })
})