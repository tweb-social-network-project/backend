'use strict'

const chai = require('chai')
const chaiAsPromised = require('chai-as-promised')

chai.use(chaiAsPromised)
chai.should()

const TestHelper = require('./helper.js')
const LikeManager = require('../repositories/like.js')
const PostManager = require('../repositories/post.js')
const UserRepository = require('../repositories/user.js')

// configure database connection
const neo4j = require('neo4j-driver').v1
const graphenedbURL = 'bolt://localhost:7687'
const graphenedbUser = 'neo4j'
const graphenedbPass = '12345'
const driver = neo4j.driver(
  graphenedbURL,
  neo4j.auth.basic(graphenedbUser, graphenedbPass),
  { disableLosslessIntegers: true }
)

const helper = new TestHelper(driver)
const manager = new LikeManager(driver)
const postManager = new PostManager(driver)
const userRepo = new UserRepository(driver)

describe('Like manager', () => {

    describe('Add like', () => {
        
        afterEach(done => {
            helper.cleanDatabase()
                .then(() => {
                    done()
                })
        })

        it('should fail if user id is missing', () => {
            manager.addLike().should.be.rejected
        })

        it('should fail if node id is missing', done => {
            manager.addLike('dummy').should.be.rejected.and.notify(done)
        })

        it('should fail if user does not exist', done => {
            manager.addLike('unknown', 'dummy').should.be.rejected.and.notify(done)
        })

        it('should fail if node does not exist', done => {
            userRepo.create({ email: 'bob@marley.com', passwordHash: '12345' })
                .then((user) => {
                    manager.addLike(user.id, 'unknown').should.be.rejected.and.notify(done)
                })
        })

        it('should succeed if user and node exist', done => {
            userRepo.create({ email: 'bob@marley.com', passwordHash: '12345' })
                .then((user) => {
                    postManager.create({ content: 'some content' }, user.id)
                        .then((post) => {
                            return manager.addLike(user.id, post.id)
                        })
                        .then(() => {
                            done()
                        })
                    })
        })

        it('should fail if user already liked the node', done => {
            userRepo.create({ email: 'bob@marley.com', passwordHash: '12345' })
                .then((user) => {
                    postManager.create({ content: 'some content' }, user.id)
                        .then((post) => {
                            manager.addLike(user.id, post.id)
                                .then(() => {
                                    manager.addLike(user.id, post.id).should.be.rejected.and.notify(done)
                                })
                        })
                    })
        })
    })

    describe('Get liked items', () => {
        
        afterEach((done) => {
            helper.cleanDatabase()
                .then(() => {
                    done()
                })
        })

        it('should fail if user id is missing', () => {
            manager.getLikedItems().should.be.rejected
        })

        it('should fail if user does not exist', (done) => {
            manager.getLikedItems('unknown').should.be.rejected
            done()
        })

        it('should return empty array if nothing liked yet', (done) => {
            userRepo.create({ email: 'bob@marley.com', passwordHash: '12345' })
                .then((user) => {
                    return manager.getLikedItems(user.id)
                })
                .then((likes) => {
                    likes.should.be.instanceof(Array)
                    likes.should.be.empty
                    done()
                })
        })

        it('should return non-empty array if something liked', (done) => {
            userRepo.create({ email: 'bob@marley.com', passwordHash: '12345' })
                .then((user) => {
                    postManager.create({ content: 'some content' }, user.id)
                        .then((post) => {
                            manager.addLike(user.id, post.id)
                                .then(() => {
                                    manager.getLikedItems(user.id)
                                        .then((likes) => {
                                            likes.should.be.instanceof(Array)
                                            likes.should.not.be.empty
                                            likes[0].id.should.equal(post.id)
                                            likes[0].type.should.equal('Post')
                                            done()
                                        })
                                })
                        })
                    })
        })
    })

    describe('Get likers', () => {
        
        afterEach((done) => {
            helper.cleanDatabase()
                .then(() => {
                    done()
                })
        })

        it('should fail if node id is missing', () => {
            manager.getLikers().should.be.rejected
        })

        it('should fail if node does not exist', (done) => {
            manager.getLikers('unknown').should.be.rejected
            done()
        })

        it('should return empty array if nobody liked yet', (done) => {
            userRepo.create({ email: 'bob@marley.com', passwordHash: '12345' })
                .then((user) => {
                    return postManager.create({ content: 'some content' }, user.id)
                })
                .then((post) => {
                    return manager.getLikers(post.id)
                })
                .then((likers) => {
                    likers.should.be.instanceof(Array)
                    likers.should.be.empty
                    done()
                })
        })

        it('should return non-empty array if someone liked', (done) => {
            userRepo.create({ email: 'bob@marley.com', passwordHash: '12345' })
                .then((user) => {
                    postManager.create({ content: 'some content' }, user.id)
                        .then((post) => {
                            manager.addLike(user.id, post.id)
                                .then(() => {
                                    manager.getLikers(post.id)
                                        .then((likers) => {
                                            likers.should.be.instanceof(Array)
                                            likers.should.not.be.empty
                                            likers[0].id.should.equal(user.id)
                                            done()
                                        })
                                })
                        })
                    })
        })
    })
})