'use strict'

const chai = require('chai')
const chaiAsPromised = require('chai-as-promised')

chai.use(chaiAsPromised)
chai.should()

const TestHelper = require('./helper.js')
const InterestManager = require('../repositories/interest.js')
const UserRepository = require('../repositories/user.js')

// configure database connection
const neo4j = require('neo4j-driver').v1
const graphenedbURL = 'bolt://localhost:7687'
const graphenedbUser = 'neo4j'
const graphenedbPass = '12345'
const driver = neo4j.driver(
  graphenedbURL,
  neo4j.auth.basic(graphenedbUser, graphenedbPass),
  { disableLosslessIntegers: true }
)

const helper = new TestHelper(driver)
const manager = new InterestManager(driver)
const userRepo = new UserRepository(driver)

describe('Interest manager', () => {

    describe('Add interest', () => {
        
        afterEach(done => {
            helper.cleanDatabase()
                .then(() => {
                    done()
                })
        })

        it('should fail if source id is missing', () => {
            manager.addInterest().should.be.rejected
        })

        it('should fail if target id is missing', done => {
            manager.addInterest('invalid').should.be.rejected.and.notify(done)
        })

        it('should fail if source node does not exist', done => {
            userRepo.create({ email: 'bob@marley.com', passwordHash: '12345' })
            .then((user) => {
                manager.addInterest('invalid', user.id).should.be.rejected.and.notify(done)
            })
        })

        it('should fail if target node does not exist', done => {
            userRepo.create({ email: 'bob@marley.com', passwordHash: '12345' })
            .then((user) => {
                manager.addInterest(user.id, 'invalid').should.be.rejected.and.notify(done)
            })
        })

        it('should return an object with an id property if source and target nodes exist', done => {
            userRepo.create({ email: 'bob@marley.com', passwordHash: '12345' })
            .then((user1) => {
                userRepo.create({ email: 'damian@marley.com', passwordHash: '12345' })
                .then((user2) => {
                    manager.addInterest(user1.id, user2.id)
                    .then((relationship) => {
                        relationship.should.have.property('id')
                        done()
                    })
                })
            })
        })

        it('should return an object with specified properties', done => {
            userRepo.create({ email: 'bob@marley.com', passwordHash: '12345' })
            .then((user1) => {
                userRepo.create({ email: 'damian@marley.com', passwordHash: '12345' })
                .then((user2) => {
                    manager.addInterest(user1.id, user2.id, { level: 'high' })
                    .then((relationship) => {
                        relationship.should.have.property('level', 'high')
                        done()
                    })
                })
            })
        })

        it('should fail if relationship already exist', done => {
            userRepo.create({ email: 'bob@marley.com', passwordHash: '12345' })
                .then((user1) => {
                    userRepo.create({ email: 'damian@marley.com', passwordHash: '12345' })
                        .then((user2) => {
                            manager.addInterest(user1.id, user2.id)
                                .then(() => {
                                    manager.addInterest(user1.id, user2.id).should.be.rejected.and.notify(done)
                                })
                        })
                })
        })
    })

    describe('Update interest', () => {
        
        afterEach((done) => {
            helper.cleanDatabase()
                .then(() => {
                    done()
                })
        })

        it('should fail if relationship id is missing', () => {
            manager.updateInterest({ other: 'some value' }).should.be.rejected
        })

        it('should fail if relationship does not exist', done => {
            manager.updateInterest({ id: 'invalid' }).should.be.rejected
            done()
        })

        it('should update an existing relationship', done => {
            userRepo.create({ email: 'bob@marley.com', passwordHash: '12345' })
                .then((user1) => {
                    userRepo.create({ email: 'damian@marley.com', passwordHash: '12345' })
                        .then((user2) => {
                            manager.addInterest(user1.id, user2.id)
                                .then((relationship) => {
                                    relationship.level = 'high'
                                    return manager.updateInterest(relationship)
                                })
                                .then((updatedRelationship) => {
                                    updatedRelationship.should.have.property('level', 'high')
                                    done()
                                })
                        })
                })
        })
    })

    describe('Remove interest', () => {
        
        afterEach((done) => {
            helper.cleanDatabase()
                .then(() => {
                    done()
                })
        })

        it('should fail if relationship id is missing', () => {
            manager.removeInterest({ other: 'some value' }).should.be.rejected
        })

        it('should succeed if relationship does not exist', (done) => {
            manager.removeInterest({ id: 'invalid' })
            .then(() => {
                done()
            })
        })

        it('should remove an existing relationship', (done) => {
            userRepo.create({ email: 'bob@marley.com', passwordHash: '12345' })
            .then((user1) => {
                userRepo.create({ email: 'damian@marley.com', passwordHash: '12345' })
                .then((user2) => {
                    manager.addInterest(user1.id, user2.id)
                    .then((relationship) => {
                        manager.removeInterest(relationship)
                        .then(() => {
                            done()
                        })
                    })
                })
            })
        })
    })

    describe('Get interests', () => {
        
        afterEach((done) => {
            helper.cleanDatabase()
                .then(() => {
                    done()
                })
        })

        it('should fail if source id is missing', () => {
            manager.getInterests().should.be.rejected
        })

        it('should return an empty array if source node does not exist', (done) => {
            manager.getInterests('invalid')
            .then((interests) => {
                interests.should.be.instanceof(Array)
                interests.should.be.empty
                done()
            })
        })

        it('should return an empty array if source has no interest', (done) => {
            userRepo.create({ email: 'bob@marley.com', passwordHash: '12345' })
            .then((user) => {
                manager.getInterests(user.id)
                .then((interests) => {
                    interests.should.be.instanceof(Array)
                    interests.should.be.empty
                    done()
                })
            })
        })

        it('should return a non-empty array if source has interests', (done) => {
            userRepo.create({ email: 'bob@marley.com', passwordHash: '12345' })
            .then((user1) => {
                userRepo.create({ email: 'damian@marley.com', passwordHash: '12345' })
                .then((user2) => {
                    manager.addInterest(user1.id, user2.id)
                    .then(() => {
                        manager.getInterests(user1.id)
                        .then((interests) => {
                            interests.should.be.instanceof(Array)
                            interests.should.not.be.empty
                            interests[0].should.have.property('type')
                            interests[0].should.not.have.property('passwordHash')
                            done()
                        })
                    })
                })
            })
        })
    })
    
    describe('Get interest count', () => {
        
        afterEach((done) => {
            helper.cleanDatabase()
                .then(() => {
                    done()
                })
        })

        it('should fail if source id is missing', () => {
            manager.getInterestCount().should.be.rejected
        })

        it('should return zero if no interests', done => {
            manager.getInterestCount('unknown')
                .then((interests) => {
                    interests.should.equal(0)
                    done()
                })
        })

        it('should return a non-empty array if target has interests', done => {
            userRepo.create({ email: 'bob@marley.com', passwordHash: '12345' })
                .then((user1) => {
                    userRepo.create({ email: 'damian@marley.com', passwordHash: '12345' })
                        .then((user2) => {
                            manager.addInterest(user1.id, user2.id)
                                .then(() => {
                                    manager.getInterestCount(user1.id)
                                        .then((interests) => {
                                            interests.should.equal(1)
                                            done()
                                        })
                                })
                        })
                })
        })
    })

    describe('Get followers', () => {
        
        afterEach((done) => {
            helper.cleanDatabase()
                .then(() => {
                    done()
                })
        })

        it('should fail if target id is missing', () => {
            manager.getFollowers().should.be.rejected
        })

        it('should return an empty array if target node does not exist', (done) => {
            manager.getFollowers('invalid')
            .then((followers) => {
                followers.should.be.instanceof(Array)
                followers.should.be.empty
                done()
            })
        })

        it('should return an empty array if target has no follower', (done) => {
            userRepo.create({ email: 'bob@marley.com', passwordHash: '12345' })
            .then((user) => {
                manager.getFollowers(user.id)
                .then((followers) => {
                    followers.should.be.instanceof(Array)
                    followers.should.be.empty
                    done()
                })
            })
        })

        it('should return a non-empty array if target has followers', (done) => {
            userRepo.create({ email: 'bob@marley.com', passwordHash: '12345' })
            .then((user1) => {
                userRepo.create({ email: 'damian@marley.com', passwordHash: '12345' })
                .then((user2) => {
                    manager.addInterest(user1.id, user2.id)
                    .then(() => {
                        manager.getFollowers(user2.id)
                            .then((followers) => {
                                followers.should.be.instanceof(Array)
                                followers.should.not.be.empty
                                followers[0].should.have.property('type')
                                followers[0].should.not.have.property('passwordHash')
                                done()
                            })
                    })
                })
            })
        })
    })
    
    describe('Get follower count', () => {
        
        afterEach((done) => {
            helper.cleanDatabase()
                .then(() => {
                    done()
                })
        })

        it('should fail if target id is missing', () => {
            manager.getFollowerCount().should.be.rejected
        })

        it('should return zero if no followers', done => {
            manager.getFollowerCount('unknown')
                .then((followers) => {
                    followers.should.equal(0)
                    done()
                })
        })

        it('should return a non-empty array if target has followers', done => {
            userRepo.create({ email: 'bob@marley.com', passwordHash: '12345' })
                .then((user1) => {
                    userRepo.create({ email: 'damian@marley.com', passwordHash: '12345' })
                        .then((user2) => {
                            manager.addInterest(user1.id, user2.id)
                                .then(() => {
                                    manager.getFollowerCount(user2.id)
                                        .then((followers) => {
                                            followers.should.equal(1)
                                            done()
                                        })
                                })
                        })
                })
        })
    })
})