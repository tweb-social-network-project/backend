'use strict'

const chai = require('chai')
const chaiAsPromised = require('chai-as-promised')

chai.use(chaiAsPromised)
chai.should()

const TestHelper = require('./helper.js')
const TagManager = require('../repositories/tag.js')
const PostManager = require('../repositories/post.js')
const UserRepository = require('../repositories/user.js')

// configure database connection
const neo4j = require('neo4j-driver').v1
const graphenedbURL = 'bolt://localhost:7687'
const graphenedbUser = 'neo4j'
const graphenedbPass = '12345'
const driver = neo4j.driver(
  graphenedbURL,
  neo4j.auth.basic(graphenedbUser, graphenedbPass),
  { disableLosslessIntegers: true }
)

const helper = new TestHelper(driver)
const manager = new TagManager(driver)
const postManager = new PostManager(driver)
const userRepo = new UserRepository(driver)

describe('Tag manager', () => {

    describe('Check if tag exist', () => {
        
        afterEach((done) => {
            helper.cleanDatabase()
                .then(() => {
                    done()
                })
        })

        it('should fail if tag is empty', () => {
            manager.checkIfExist().should.be.rejected
        })

        it('should return false if tag does not exist', (done) => {
            manager.checkIfExist('unknown')
                .then((exist) => {
                    exist.should.be.false
                    done()
                })
        })

        it('should return true if tag exist', (done) => {
            manager.create('#awesome')
                .then((tag) => {
                    manager.checkIfExist(tag.name)
                    .then((exist) => {
                        exist.should.be.true
                        done()
                    })
                })           
        })
    })

    describe('Get tag by name', () => {
        
        afterEach((done) => {
            helper.cleanDatabase()
                .then(() => {
                    done()
                })
        })

        it('should fail if tag is empty', () => {
            manager.getTagByName().should.be.rejected
        })

        it('should return false if tag does not exist', (done) => {
            manager.getTagByName('unknown').should.be.rejected
            done()
        })

        it('should return true if tag exist', (done) => {
            manager.create('#awesome')
                .then((tag) => {
                    return manager.getTagByName(tag.name)
                })   
                .then((tag) => {
                    tag.should.have.property('id')
                    done()
                })        
        })
    })

    describe('Create tag', () => {
        
        afterEach((done) => {
            helper.cleanDatabase()
                .then(() => {
                    done()
                })
        })

        it('should fail if tag is empty', () => {
            manager.create().should.be.rejected
        })


        it('should fail if tag does not start with hashtag', () => {
            manager.create('badformat').should.be.rejected
        })

        it('should succeed if tag starts with hashtag', (done) => {
            manager.create('#awesome')
                .then((tag) => {
                    tag.should.have.property('id')
                    done()
                })
        })

        it('should fail if tag already exist', (done) => {
            manager.create('#awesome')
                .then(() => {
                    manager.create('#awesome').should.be.rejected
                    done()
                })
        })
    })    

    describe('Add reference from node', () => {
        
        afterEach((done) => {
            helper.cleanDatabase()
                .then(() => {
                    done()
                })
        })

        it('should fail if tag is missing', () => {
            manager.addReference().should.be.rejected
        })

        it('should fail if node id is missing', () => {
            manager.addReference('dummy').should.be.rejected
        })

        it('should fail if tag does not exist', (done) => {
            userRepo.create({ email: 'bob@marley.com', passwordHash: '12345' })
                .then((user) => {
                    postManager.create({ content: 'some content' }, user.id)
                        .then((post) => {
                            manager.addReference('unknown', post.id).should.be.rejected
                            done()
                        })
                })
        })

        it('should fail if node does not exists', (done) => {
            manager.create('#awesome')
                .then((tag) => {
                    manager.addReference(tag.name, 'unknown').should.be.rejected
                    done()
                })
        })

        it('should succeed if tag and node exist', (done) => {
            userRepo.create({ email: 'bob@marley.com', passwordHash: '12345' })
                .then((user) => {
                    postManager.create({ content: 'some content' }, user.id)
                        .then((post) => {
                            manager.create('#awesome')
                                .then((tag) => {
                                    manager.addReference(tag.name, post.id)
                                        .then(() => {
                                            done()
                                        })
                                })
                        })
                })
        })
    })

    describe('Get referencing nodes', () => {
        
        afterEach((done) => {
            helper.cleanDatabase()
                .then(() => {
                    done()
                })
        })

        it('should fail if tag is empty', () => {
            manager.getReferencingNodes().should.be.rejected
        })

        it('should return an empty array if tag does not exist', (done) => {
            manager.getReferencingNodes('unknown')
                .then((nodes) => {
                    nodes.should.be.instanceof(Array)
                    nodes.should.be.empty
                    done()
                })
        })

        it('should return a non-empty array if tag exist', (done) => {
            userRepo.create({ email: 'bob@marley.com', passwordHash: '12345' })
                .then((user) => {
                    postManager.create({ content: 'some content' }, user.id)
                        .then((post) => {
                            manager.create('#awesome')
                                .then((tag) => {
                                    manager.addReference(tag.name, post.id)
                                        .then(() => {
                                            manager.getReferencingNodes(tag.name)
                                                .then((nodes) => {
                                                    nodes.should.be.instanceof(Array)
                                                    nodes.should.not.be.empty
                                                    nodes[0].should.have.property('type', 'Post')
                                                    done()
                                                })
                                        })
                                })
                        })
                })      
        })
    })

    describe('Get all tags', () => {
        
        afterEach((done) => {
            helper.cleanDatabase()
                .then(() => {
                    done()
                })
        })

        it('should return an empty array if no tag exist', (done) => {
            manager.getAll()
                .then((tags) => {
                    tags.should.be.instanceof(Array)
                    tags.should.be.empty
                    done()
                })
        })

        it('should return a non-empty array if tags exist', (done) => {
            manager.create('#awesome')
                .then(() => {
                    return manager.getAll()
                })
                .then((tags) => {
                    tags.should.be.instanceof(Array)
                    tags.should.not.be.empty
                    done()
                })
        })
    })

    describe('Find tags', () => {
        
        afterEach((done) => {
            helper.cleanDatabase()
                .then(() => {
                    done()
                })
        })

        it('should fail if word is missing', () => {
            manager.findByWord().should.be.rejected
        })

        it('should return an empty array if no tags are found', (done) => {
            manager.create('#awesome')
                .then(() => {
                    return manager.findByWord('unknown')
                })
                .then((tags) => {
                    tags.should.be.instanceof(Array)
                    tags.should.be.empty
                    done()
                })
        })

        it('should return a non-empty array if tags are found', (done) => {
            manager.create('#gardening')
                .then(() => {
                    return manager.findByWord('garden')
                })
                .then((tags) => {
                    tags.should.be.instanceof(Array)
                    tags.should.not.be.empty
                    tags[0].name.should.equal('#gardening')
                    done()
                })
        })
    })
})