'use strict'

const chai = require('chai')
const chaiAsPromised = require('chai-as-promised')

chai.use(chaiAsPromised)
chai.should()

const TestHelper = require('./helper.js')
const DashboardManager = require('../repositories/dashboard.js')
const InterestManager = require('../repositories/interest.js')
const TagManager = require('../repositories/tag.js')
const LikeManager = require('../repositories/like.js')
const PostManager = require('../repositories/post.js')
const UserRepository = require('../repositories/user.js')

// configure database connection
const neo4j = require('neo4j-driver').v1
const graphenedbURL = 'bolt://localhost:7687'
const graphenedbUser = 'neo4j'
const graphenedbPass = '12345'
const driver = neo4j.driver(
  graphenedbURL,
  neo4j.auth.basic(graphenedbUser, graphenedbPass),
  { disableLosslessIntegers: true }
)

const helper = new TestHelper(driver)
const manager = new DashboardManager(driver)
const interestManager = new InterestManager(driver)
const tagManager = new TagManager(driver)
const likeManager = new LikeManager(driver)
const postManager = new PostManager(driver)
const userRepo = new UserRepository(driver)

describe('Dashboard manager', () => {

    describe('Get subscribed posts', () => {
        
        afterEach((done) => {
            helper.cleanDatabase()
                .then(() => {
                    done()
                })
        })

        it('should fail if user id is missing', () => {
            manager.getSubscribedPosts().should.be.rejected
        })

        it('should fail if user does not exist', (done) => {
            manager.getSubscribedPosts('unknown').should.be.rejected
            done()
        })

        it('should return empty array if user follows nobody', (done) => {
            userRepo.create({ email: 'bob@marley.com', passwordHash: '12345' })
                .then((user) => {
                    return manager.getSubscribedPosts(user.id)
                })
                .then((posts) => {
                    posts.should.be.instanceof(Array)
                    posts.should.be.empty
                    done()
                })
        })

        it('should return empty array if user follows someone who has not posted yet', (done) => {
            userRepo.create({ email: 'bob@marley.com', passwordHash: '12345' })
                .then((user1) => {
                    userRepo.create({ email: 'john@marley.com', passwordHash: '12345' })
                        .then((user2) => {
                            return interestManager.addInterest(user1.id, user2.id)
                        })
                        .then(() => {
                            return manager.getSubscribedPosts(user1.id)
                        })
                        .then((posts) => {
                            posts.should.be.instanceof(Array)
                            posts.should.be.empty
                            done()
                        })
                })
        })

        it('should return non-empty array if user follows someone who has posted', (done) => {
            userRepo.create({ email: 'bob@marley.com', passwordHash: '12345' })
                .then((user1) => {
                    userRepo.create({ email: 'john@marley.com', passwordHash: '12345' })
                        .then((user2) => {
                            postManager.create({ content: 'some #awesome content' }, user2.id)
                                .then(() => {
                                    return interestManager.addInterest(user1.id, user2.id)
                                })
                                .then(() => {
                                    return manager.getSubscribedPosts(user1.id)
                                })
                                .then((posts) => {
                                    posts.should.be.instanceof(Array)
                                    posts.should.not.be.empty
                                    done()
                                })
                            })
                })
        })

        it('should return posts ordered by created date (descending)', (done) => {
            userRepo.create({ email: 'bob@marley.com', passwordHash: '12345' })
                .then((user1) => {
                    userRepo.create({ email: 'john@marley.com', passwordHash: '12345' })
                        .then((user2) => {
                            postManager.create({ content: 'some #awesome content 1' }, user2.id)
                                .then(() => {
                                    return postManager.create({ content: 'some #awesome content 2' }, user2.id)
                                })
                                .then(() => {
                                    return interestManager.addInterest(user1.id, user2.id)
                                })
                                .then(() => {
                                    return manager.getSubscribedPosts(user1.id)
                                })
                                .then((posts) => {
                                    posts.should.be.instanceof(Array)
                                    posts.should.not.be.empty
                                    posts[0].post.content.should.equal('some #awesome content 2')
                                    posts[1].post.content.should.equal('some #awesome content 1')
                                    done()
                                })
                            })
                })
        })
    })

    describe('Get interesting posts', () => {
        
        afterEach((done) => {
            helper.cleanDatabase()
                .then(() => {
                    done()
                })
        })

        it('should fail if user id is missing', () => {
            manager.getInterestingPosts().should.be.rejected
        })

        it('should fail if user does not exist', (done) => {
            manager.getInterestingPosts('unknown').should.be.rejected
            done()
        })

        it('should return empty array if user is not interested in tags', (done) => {
            userRepo.create({ email: 'bob@marley.com', passwordHash: '12345' })
                .then((user1) => {
                    return postManager.create({ content: 'some #awesome content' }, user1.id)
                })
                .then(() => {
                    return userRepo.create({ email: 'john@marley.com', passwordHash: '12345' })
                })
                .then((user2) => {
                    return manager.getInterestingPosts(user2.id)
                })
                .then((posts) => {
                    posts.should.be.instanceof(Array)
                    posts.should.be.empty
                    done()
                })
        })

        it('should return empty array if no post contains a tag the user is interested in', (done) => {
            userRepo.create({ email: 'bob@marley.com', passwordHash: '12345' })
                .then((user1) => {
                    return postManager.create({ content: 'some #awesome content' }, user1.id)
                })
                .then(() => {
                    return userRepo.create({ email: 'john@marley.com', passwordHash: '12345' })
                        .then((user2) => {
                            tagManager.create('#super')
                                .then((tag) => {
                                    return interestManager.addInterest(user2.id, tag.id)
                                })
                                .then(() => {
                                    return manager.getInterestingPosts(user2.id)
                                })
                                .then((posts) => {
                                    posts.should.be.instanceof(Array)
                                    posts.should.be.empty
                                    done()
                                })
                            })
                        })
        })

        it('should return non-empty array if a post contains a tag the user is interested in', (done) => {
            userRepo.create({ email: 'bob@marley.com', passwordHash: '12345' })
                .then((user1) => {
                    return postManager.create({ content: 'some #awesome content' }, user1.id)
                })
                .then(() => {
                    return userRepo.create({ email: 'john@marley.com', passwordHash: '12345' })
                        .then((user2) => {
                            tagManager.getTagByName('#awesome')
                                .then((tag) => {
                                    return interestManager.addInterest(user2.id, tag.id)
                                })
                                .then(() => {
                                    return manager.getInterestingPosts(user2.id)
                                })
                                .then((posts) => {
                                    posts.should.be.instanceof(Array)
                                    posts.should.not.be.empty
                                    posts[0].should.have.property('post')
                                    posts[0].should.have.property('author')
                                    posts[0].should.have.property('tag')
                                    posts[0].likes.should.equal(0)
                                    done()
                                })
                            })
                        })
        })

        it('should return posts ordered by # of likes (descending)', (done) => {
            userRepo.create({ email: 'damian@marley.com', passwordHash: '12345' })
                .then((user0) => {
                    userRepo.create({ email: 'bob@marley.com', passwordHash: '12345' })
                        .then((user1) => {
                            postManager.create({ content: 'some #good content' }, user1.id)
                                .then((post1) => {
                                    postManager.create({ content: 'some #great content' }, user1.id)
                                        .then((post2) => {
                                            likeManager.addLike(user1.id, post1.id) // 1 like for post 1
                                                .then(() => {
                                                    return likeManager.addLike(user1.id, post2.id)
                                                })
                                                .then(() => {
                                                    return likeManager.addLike(user0.id, post2.id) // 2 likes for post 2
                                                })
                                                .then(() => {
                                                    userRepo.create({ email: 'john@marley.com', passwordHash: '12345' })
                                                        .then((user2) => {
                                                            tagManager.getTagByName('#good')
                                                                .then((tag1) => {
                                                                    return interestManager.addInterest(user2.id, tag1.id)
                                                                })
                                                                .then(() => {
                                                                    return tagManager.getTagByName('#great')
                                                                })
                                                                .then((tag2) => {
                                                                    return interestManager.addInterest(user2.id, tag2.id)
                                                                })
                                                                .then(() => {
                                                                    return manager.getInterestingPosts(user2.id)
                                                                })
                                                                .then((posts) => {
                                                                    posts.should.be.instanceof(Array)
                                                                    posts.should.not.be.empty
                                                                    posts[0].should.have.property('post')
                                                                    posts[0].should.have.property('author')
                                                                    posts[0].should.have.property('tag')
                                                                    posts[0].likes.should.equal(2)
                                                                    posts[1].likes.should.equal(1)
                                                                    done()
                                                                })
                                                            })
                                                        })
                                                    })
                                })
                        })
                    })
        })
    })

    describe('Get trending posts', () => {
        
        afterEach((done) => {
            helper.cleanDatabase()
                .then(() => {
                    done()
                })
        })

        it('should return an empty array if nobody posted', done => {
            manager.getTrendingPosts()
                .then((posts) => {
                    posts.should.be.instanceof(Array)
                    posts.should.be.empty
                    done()
                })
        })        

        it('should return non-empty array if someone posted', done => {
            userRepo.create({ email: 'bob@marley.com', passwordHash: '12345' })
                .then((user1) => {
                    return postManager.create({ content: 'some #awesome content' }, user1.id)
                })
                .then(() => {
                    return manager.getTrendingPosts()
                })
                .then((posts) => {
                    posts.should.be.instanceof(Array)
                    posts.should.not.be.empty
                    done()
                })
        })

        it('should return posts ordered by # of answers (descending) and then by # of likes (descending)', done => {
            userRepo.create({ email: 'damian@marley.com', passwordHash: '12345' })
                .then((user0) => {
                    userRepo.create({ email: 'bob@marley.com', passwordHash: '12345' })
                        .then((user1) => {
                            postManager.create({ content: 'some #good content' }, user1.id)
                                .then((post1) => {
                                    postManager.create({ content: 'some #great content' }, user1.id, post1.id)
                                        .then((post2) => {
                                            likeManager.addLike(user1.id, post1.id) // 1 like for post 1
                                                .then(() => {
                                                    return likeManager.addLike(user1.id, post2.id)
                                                })
                                                .then(() => {
                                                    return likeManager.addLike(user0.id, post2.id) // 2 likes for post 2
                                                })
                                                .then(() => {
                                                    return manager.getTrendingPosts()
                                                })
                                                .then((posts) => {
                                                    posts.should.be.instanceof(Array)
                                                    posts.should.not.be.empty
                                                    posts[0].should.have.property('post')
                                                    posts[0].should.have.property('author')
                                                    posts[0].answers.should.equal(1)
                                                    posts[1].answers.should.equal(0)
                                                    posts[0].likes.should.equal(1)
                                                    posts[1].likes.should.equal(2)
                                                    done()
                                                })
                                            })
                                })
                        })
                    })
        })
    })
})