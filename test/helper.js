'use strict'

class TestHelper {
    constructor(driver) {
      this.driver = driver
    }

    cleanDatabase() {
        return new Promise((resolve, reject) => {
            const cypher = 'MATCH (n) DETACH DELETE n'
            const session = this.driver.session()
            session
                .run(cypher)
                .then(() => {
                    session.close()
                    resolve()
                })
                .catch((error) => {
                    reject(error)
                })
        })
    }
}

module.exports = TestHelper