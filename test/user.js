'use strict'

/* eslint-disable no-undef */
/* eslint-disable import/no-extraneous-dependencies */
const chai = require('chai')
const chaiAsPromised = require('chai-as-promised')

chai.use(chaiAsPromised)
chai.should()

const TestHelper = require('./helper.js')
const UserRepository = require('../repositories/user.js')
const PostManager = require('../repositories/post.js')

// configure database connection
const neo4j = require('neo4j-driver').v1
const graphenedbURL = 'bolt://localhost:7687'
const graphenedbUser = 'neo4j'
const graphenedbPass = '12345'
const driver = neo4j.driver(
	graphenedbURL,
	neo4j.auth.basic(graphenedbUser, graphenedbPass),
	{ disableLosslessIntegers: true }
)

const helper = new TestHelper(driver)
const repo = new UserRepository(driver)
const postManager = new PostManager(driver)

describe('User repository', () => {

	describe('Create user', () => {
        
        afterEach((done) => {
            helper.cleanDatabase()
                .then(() => {
                    done()
                })
		})

		it('should fail if email hash is missing', () => {
			repo.create({ passwordHash: 'some value' }).should.be.rejected
		})

		it('should fail if password hash is missing', () => {
			repo.create({ email: 'some email' }).should.be.rejected
		})

		it('should return an object with an id property if all required fields are specified', (done) => {
			repo.create({ email: 'bob@marley.com', passwordHash: '12345' })
				.then((user) => {
					user.should.have.property('id')
					done()
				})
		})
	})

	describe('Get users', () => {
        
        afterEach((done) => {
            helper.cleanDatabase()
                .then(() => {
                    done()
                })
		})

		it('should return an empty array if no user exist', (done) => {
			repo.getAll()
				.then((users) => {
					users.should.be.instanceof(Array)
					users.should.be.empty
					done()
				})
		})

		it('should return a non-empty array if users exist', (done) => {
			repo.create({ email: 'bob@marley.com', passwordHash: '12345' })
				.then(() => {
					return repo.getAll()
				})
				.then((users) => {
					users.should.be.instanceof(Array)
					users.should.not.be.empty
					done()
				})
		})

		it('should not return passwords', (done) => {
			repo.create({ email: 'bob@marley.com', passwordHash: '12345' })
				.then(() => {
					return repo.getAll()
				})
				.then((users) => {
					users.should.be.instanceof(Array)
					users.should.not.be.empty
					users[0].should.not.have.property('passwordHash')
					done()
				})
		})
	})

	describe('Check email', () => {
        
        afterEach((done) => {
            helper.cleanDatabase()
                .then(() => {
                    done()
                })
		})

		it('should return false is email is not used', (done) => {
			repo.isEmailUsed('damian@marley.com')
				.then((used) => {
					used.should.be.false
					done()
				})
		})

		it('should return true is email is used', (done) => {
			repo.create({ email: 'bob@marley.com', passwordHash: '12345' })
				.then((user) => {
					return repo.isEmailUsed(user.email)
				})
				.then((used) => {
					used.should.be.true
					done()
				})
		})
	})

	describe('Get password hash', () => {
        
        afterEach((done) => {
            helper.cleanDatabase()
                .then(() => {
                    done()
                })
		})

		it('should fail if email address does not exist', () => {
			repo.getPasswordHashByEmail('unknown').should.be.rejected
		})

		it('should return password hash if email address exist', (done) => {
			repo.create({ email: 'bob@marley.com', passwordHash: '12345' })
				.then(() => {
					return repo.getPasswordHashByEmail('bob@marley.com')
				})
				.then(() => {
					done()
				})
		})

		it('should return password hash if email address exist but case is wrong', (done) => {
			repo.create({ email: 'bob@marley.com', passwordHash: '12345' })
				.then(() => {
					return repo.getPasswordHashByEmail('BOB@marley.com')
				})
				.then(() => {
					done()
				})
		})
	})

	describe('Get user by email', () => {
		afterEach((done) => {
			helper.cleanDatabase()
				.then(() => {
					done()
				})
		})

		it('should fail if email address is missing', () => {
			repo.getUserByEmail().should.be.rejected
		})

		it('should fail if the email address does not exist', () => {
			repo.getUserByEmail('unknown').should.be.rejected
		})

		it('should return a user if the email exists', (done) => {
			repo.create({ email: 'bob@marley.com', passwordHash: '12345' })
				.then(() => {
					return repo.getUserByEmail('bob@marley.com')
				})
				.then((user) => {
					user.should.have.property('id')
					done()
				})
		})
	})

	describe('Update user', () => {
        
        afterEach((done) => {
            helper.cleanDatabase()
                .then(() => {
                    done()
                })
		})

		it('should fail if id property is missing', () => {
			repo.update({ age: 42 }).should.be.rejected
		})

		it('should fail if user does not exist', () => {
			repo.update({ id: 'unknown' }).should.be.rejected
		})

		it('should update existing attribute', (done) => {
			repo.create({ email: 'bob@marley.com', passwordHash: '12345' })
				.then((user) => {
					user.passwordHash = '6789'
					repo.update(user)
						.then((updated_user) => {
							updated_user.should.have.property('passwordHash', '6789')
							done()
						})
				})
		})

		it('should add new attribute', (done) => {
			repo.create({ email: 'bob@marley.com', passwordHash: '12345' })
				.then((user) => {
					user.age = 42
					repo.update(user)
						.then((updated_user) => {
							updated_user.should.have.property('age', 42)
							done()
						})
				})
		})

		it('should not remove password hash', (done) => {
			repo.create({ email: 'bob@marley.com', passwordHash: '12345' })
				.then((user) => {
					user.age = 42
					delete user.passwordHash
					repo.update(user)
						.then((updated_user) => {
							updated_user.should.have.property('age', 42)
							updated_user.should.have.property('passwordHash', '12345')
							done()
						})
				})
		})

		it('should update password hash', (done) => {
			repo.create({ email: 'bob@marley.com', passwordHash: '12345' })
				.then((user) => {
					user.passwordHash = '67890'
					repo.update(user)
						.then((updated_user) => {
							updated_user.should.have.property('passwordHash', '67890')
							done()
						})
				})
		})
	})

	describe('Delete user', () => {
        
        afterEach((done) => {
            helper.cleanDatabase()
                .then(() => {
                    done()
                })
		})
		
		it('should succeed if id does not exist', (done) => {
			repo.delete(-1)
				.then(() => done())
		})

		it('should succeed if id exist', (done) => {
			repo.create({ email: 'bob@marley.com', passwordHash: '12345' })
				.then((user) => {
					repo.delete(user.id)
						.then(() => done())
				})
		})

		it('should succeed if relationships exist', (done) => {
			repo.create({ email: 'bob@marley.com', passwordHash: '12345' })
				.then((user) => {
					postManager.create({ content: 'some content' }, user.id)
						.then(() => {
							repo.delete(user.id)
								.then(() => done())
						})
				})
		})
	})

	describe('Get user by id', () => {
        
        afterEach((done) => {
            helper.cleanDatabase()
                .then(() => {
                    done()
                })
        })

		it('should fail if user id is missing', () => {
            repo.getById().should.be.rejected
        })

        it('should fail if user does not exist', (done) => {
			repo.getById('unknown').should.be.rejected
			done()
        })

        it('should succeed if user exist', (done) => {
            repo.create({ email: 'bob@marley.com', passwordHash: '12345' })
                .then((user) => {
                    return repo.getById(user.id)
                })
                .then((user) => {
					user.should.have.property('id', user.id)
                    done()
                })
        })
	})

	describe('Find users', () => {
        
        afterEach((done) => {
            helper.cleanDatabase()
                .then(() => {
                    done()
                })
		})

        it('should fail if word is missing', () => {
            repo.findByWord().should.be.rejected
        })

		it('should return an empty array if no user exist', (done) => {
			repo.findByWord('dummy')
				.then((users) => {
					users.should.be.instanceof(Array)
					users.should.be.empty
					done()
				})
		})

		it('should return an empty array if users exist but not with searched word', (done) => {
			repo.create({ email: 'bob@marley.com', passwordHash: '12345' , firstName: 'Bob', lastName: 'Marley'})
				.then(() => {
					return repo.findByWord('other')
				})
				.then((users) => {
					users.should.be.instanceof(Array)
					users.should.be.empty
					done()
				})
		})

		it('should return users if email local part is matching', (done) => {
			repo.create({ email: 'green@jamaica.com', passwordHash: '12345' , firstName: 'Bob', lastName: 'Marley'})
				.then(() => {
					return repo.findByWord('green')
				})
				.then((users) => {
					users.should.be.instanceof(Array)
					users.should.not.be.empty
					done()
				})
		})

		it('should not return users if email domain is matching', (done) => {
			repo.create({ email: 'green@jamaica.com', passwordHash: '12345' , firstName: 'Bob', lastName: 'Marley'})
				.then(() => {
					return repo.findByWord('jam')
				})
				.then((users) => {
					users.should.be.instanceof(Array)
					users.should.be.empty
					done()
				})
		})

		it('should return users if first name is matching', (done) => {
			repo.create({ email: 'green@jamaica.com', passwordHash: '12345' , firstName: 'Bob', lastName: 'Marley'})
				.then(() => {
					return repo.findByWord('bob')
				})
				.then((users) => {
					users.should.be.instanceof(Array)
					users.should.not.be.empty
					done()
				})
		})

		it('should return users if last name is matching', (done) => {
			repo.create({ email: 'bob@jamaica.com', passwordHash: '12345' , firstName: 'Bob', lastName: 'Marley'})
				.then(() => {
					return repo.findByWord('ley')
				})
				.then((users) => {
					users.should.be.instanceof(Array)
					users.should.not.be.empty
					done()
				})
		})

		it('should not return passwords', (done) => {
			repo.create({ email: 'bob@jamaica.com', passwordHash: '12345' , firstName: 'Bob', lastName: 'Marley'})
				.then(() => {
					return repo.findByWord('bob')
				})
				.then((users) => {
					users.should.be.instanceof(Array)
					users.should.not.be.empty
					users[0].should.not.have.property('passwordHash')
					done()
				})
		})
	})
})

driver.close()
