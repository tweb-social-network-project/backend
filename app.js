const express = require('express');
const cors = require('cors');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const jwt = require('jsonwebtoken');
const swaggerJSDoc = require('swagger-jsdoc');
const swaggerUi = require('swagger-ui-express');

const users = require('./routes/users');
const tags = require('./routes/tags');
const interests = require('./routes/interests');
const posts = require('./routes/posts');
const authentication = require('./routes/authentication');
const config = require('./config');

require('dotenv').config();

const app = express();

const corsOptions = {
  origin: '*'
};

app.use(cors(corsOptions));

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.set('secret', config.secret);

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

const options = {
  swaggerDefinition: {
    info: {
      title: 'Agora REST API',
      version: '1.0.0',
      description: 'Agora REST API documentation'
    },
    tags: [
      {
        name: 'Agora',
        description: 'Agora API'
      }
    ],
    host: 'localhost:8081',
    basePath: '/'
  },
  apis: [
    './routes/authentication.js',
    './routes/interests.js',
    './routes/posts.js',
    './routes/tags.js',
    './routes/users.js',
    './models/user.js',
    './models/tag.js',
    './models/post.js'
  ]
};

const swaggerSpec = swaggerJSDoc(options);

app.get('/json', (req, res) => {
  res.setHeader('Content-Type', 'application/json');
  res.send(swaggerSpec);
});

app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerSpec));


app.use('/authentication', authentication);

app.use((req, res, next) => {
  const token = req.body.token || req.query.token || req.headers['x-access-token'];

  if (token) {
    jwt.verify(token, app.get('secret'), (err, decoded) => {
      if (err) {
        return res.status(403).json({ success: false, message: 'Failed to authenticate token.' });
      }
      req.decoded = decoded;
      return next();
    });
  } else {
    return res.status(403).json({
      success: false,
      message: 'No token provided.'
    });
  }
});

/**
 * Protected routes
 */

app.use('/users', users);
app.use('/tags', tags);
app.use('/interests', interests);
app.use('/posts', posts);

// catch 404 and forward to error handler
app.use((req, res, next) => { // eslint-disable-line no-unused-vars
  res.status(404).send('Not found');
});

// error handler
app.use((err, req, res, next) => { // eslint-disable-line no-unused-vars
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');

  console.log(err.message); // eslint-disable-line no-console
});

const server = app.listen(process.env.PORT || 8081, () => {
  const host = server.address().address;
  const port = server.address().port;
  console.log('Node server listening at http://%s:%s', host, port); // eslint-disable-line no-console
});
