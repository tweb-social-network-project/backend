const express = require('express');
const InterestManager = require('./../repositories/interest');
const driver = require('./../utils/driver');
const { verifyId, forbidden } = require('./../utils/secureByUserId');

const router = express.Router();

const interests = new InterestManager(driver);

/**
 * @swagger
 * /interests:
 *   post:
 *     description: Add an interest between two nodes
 *     tags:
 *       - Interests
 *     parameters:
 *       - in: body
 *         name: body
 *         type: object
 *         required : true
 *         schema:
 *          type: object
 *          properties:
 *            sourceId:
 *              type: string
 *            targetId:
 *              type: string
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: Created interest
 *         schema:
 *           type: object
 *           properties:
 *             sourceId:
 *               type: string
 *             targetId:
 *               type: string
 */
router.post('/', (req, res, next) => {
  if (!verifyId(req, req.body.sourceId)) {
    return res.status(403).json(forbidden);
  }
  interests.addInterest(req.body.sourceId, req.body.targetId, req.body.properties)
    .then(result => res.send(result))
    .catch(next);
});

/**
 * @swagger
 * /interests:
 *   put:
 *     description: Remove an interest
 *     tags:
 *       - Interests
 *     parameters:
 *       - in: body
 *         name: body
 *         requiered: true
 *         schema:
 *          type: object
 *          properties:
 *            id:
 *              type: string
 *         description: ID of the interest to remove
 *     responses:
 *       200:
 *         description: Removed
 */
router.put('/', (req, res, next) => {
  interests.removeInterest({
    id: req.body.id
  })
    .then(result => res.send(result))
    .catch(next);
});

/**
 * @swagger
 * /interests/:userId:
 *   get:
 *     description: Get user interests
 *     tags:
 *       - Interests
 *     parameters:
 *       - in: path
 *         name: userId
 *         type: string
 *         required: true
 *         description: String ID of the user.
 *     responses:
 *       200:
 *         description: User
 *         schema:
 *           type: array
 *           items:
 *             type: object
 *             properties:
 *              id:
 *                type: string
 */
router.get('/:userId', (req, res, next) => {
  interests.getInterests(req.params.userId)
    .then(result => res.send(result))
    .catch(next);
});

module.exports = router;
