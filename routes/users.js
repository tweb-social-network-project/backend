const express = require('express');
const UserRepository = require('./../repositories/user');
const InterestManager = require('./../repositories/interest');
const driver = require('./../utils/driver');
const config = require('../config.js');
const { verifyId, forbidden, getUsersId } = require('./../utils/secureByUserId');

const app = express();

app.set('secret', config.secret);

const router = express.Router();

const repo = new UserRepository(driver);
const interests = new InterestManager(driver);

/**
 * @swagger
 * /users:
 *   delete:
 *     description: Deletes a user
 *     tags:
 *       - Users
 *     parameters:
 *       - in: body
 *         name: body
 *         type: string
 *         required: true
 *         schema:
 *           type: object
 *           properties:
 *            id:
 *              type : string
 *         description: String ID of the user to delete.
 *     responses:
 *       200:
 *         description: Deleted
 */
router.delete('/', (req, res, next) => {
  if (!verifyId(req, getUsersId(req))) {
    return res.status(403).json(forbidden);
  }
  repo.delete(getUsersId(req))
    .then(result => res.send(result))
    .catch(next);
});

/**
 * @swagger
 * /users:
 *   put:
 *     description: Updates a user
 *     tags:
 *       - Users
 *     parameters:
 *       - in: body
 *         name: body
 *         type: object
 *         required: true
 *         schema:
 *           type: object
 *           properties:
 *            id:
 *              type: string
 *            firstName:
 *              type: string
 *            lastName:
 *              type: string
 *            email:
 *              type: string
 *     responses:
 *       200:
 *         description: Updated
 */
router.put('/', (req, res, next) => {
    if (!verifyId(req, req.body.id)) {
      return res.status(403).json(forbidden);
    }
    repo.update({
      id: req.body.id,
      firstName: req.body.firstName,
      lastName: req.body.lastName,
      email: req.body.email,
      avatarUrl: req.body.avatarUrl
    })
      .then(result => res.send(result))
      .catch(next);
});

/**
 * @swagger
 * /users/:id:
 *   get:
 *     description: Get a user by his ID
 *     tags:
 *       - Users
 *     parameters:
 *       - in: path
 *         name: id
 *         type: string
 *         required: true
 *         description: String ID of the user to get.
 *     responses:
 *       200:
 *         description: User
 *         schema:
 *           type: object
 *           properties:
 *            id:
 *             type: string
 *            firstName:
 *             type: string
 *            lastName:
 *             type: string
 *            email:
 *             type: string
 */
router.get('/:id', (req, res, next) => {
  repo.getUserById(req.params.id)
    .then(user => res.send({
      id: user.id,
      firstName: user.firstName,
      lastName: user.lastName,
      email: user.email
    }))
    .catch(next);
});

/**
 * @swagger
 * /users/search/:keyword:
 *   get:
 *     description: Get users with keyword in name
 *     tags:
 *       - Users
 *     parameters:
 *       - in: path
 *         name: keyword
 *         type: string
 *         required: true
 *         description: Keyword contained in user name
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: Results sent
 *         schema:
 *           type: array
 *           items:
 *              type: object
 *              properties:
 *                id:
 *                  type: string
 *                firstName:
 *                  type: string
 *                lastName:
 *                  type: string
 *                email:
 *                  type: string
 */
router.get('/search/:keyword', (req, res, next) => {
  repo.findByWord(req.params.keyword)
    .then(((result) => {
      res.send(result);
    }))
    .catch(next);
});

/**
 * @swagger
 * /users/following/:id:
 *   get:
 *     description: Get a users following count
 *     tags:
 *       - Users
 *     parameters:
 *       - in: path
 *         name: id
 *         type: string
 *         required: true
 *         description: String ID of the user.
 *     responses:
 *       200:
 *         description: Following count
 *         schema:
 *           type: object
 *           properties:
 *            count:
 *             type: number
 */
router.get('/following/:id', (req, res, next) => {
  if (!verifyId(req, req.params.id)) {
    return res.status(403).json(forbidden);
  }
  interests.getInterestCount(req.params.id)
    .then(result => res.send({
      count: result
    }))
    .catch(next);
});

/**
 * @swagger
 * /users/follower/:id:
 *   get:
 *     description: Get a users followers count
 *     tags:
 *       - Users
 *     parameters:
 *       - in: path
 *         name: id
 *         type: string
 *         required: true
 *         description: String ID of the user.
 *     responses:
 *       200:
 *         description: Followers count
 *         schema:
 *           type: object
 *           properties:
 *            count:
 *             type: number
 */
router.get('/follower/:id', (req, res, next) => {
  if (!verifyId(req, req.params.id)) {
    return res.status(403).json(forbidden);
  }
  interests.getFollowerCount(req.params.id)
    .then(result => res.send({
      count: result
    }))
    .catch(next);
});

module.exports = router;
