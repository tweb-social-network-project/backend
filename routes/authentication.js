const express = require('express');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const UserRepository = require('../repositories/user');
const driver = require('../utils/driver');
const config = require('../config.js');

const router = express.Router();

const repo = new UserRepository(driver);

const app = express();

app.set('secret', config.secret);

const saltRounds = 10;

/**
 * @swagger
 * /authentication/signup:
 *   post:
 *     description: Signs up a user
 *     tags:
 *       - Authentication
 *     parameters:
 *       - in: body
 *         name: body
 *         type: object
 *         required: true
 *         schema:
 *           type: object
 *           properties:
 *            id:
 *             type: string
 *            firstName:
 *             type: string
 *            lastName:
 *             type: string
 *            email:
 *             type: string
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: signin
 *         schema:
 *           type: object
 *           required:
 *             - success
 *             - user
 *           properties:
 *             success:
 *               type: boolean
 *             user:
 *               type: object
 *               properties:
 *                 id:
 *                   type: string
 *                 firstName:
 *                   type: string
 *                 lastName:
 *                   type: string
 *                 email:
 *                   type: string
 */
router.post('/signup', (req, res) => {
  repo.create({
      firstName: req.body.firstName,
      lastName: req.body.lastName,
      email: req.body.email,
      avatarUrl: 'default',
      passwordHash: bcrypt.hashSync(req.body.password, saltRounds)
  })
  .then((user) => {
      res.json({
        success: true,
        user: {
          id: user.id,
          firstName: user.firstName,
          lastName: user.lastName,
          email: user.email,
          avatarUrl: user.avatarUrl
        }
      });
  });
});

/**
 * @swagger
 * /authentication/signin:
 *   post:
 *     description: Signs in a user
 *     tags:
 *       - Authentication
 *     parameters:
 *       - in: body
 *         name: body
 *         type: object
 *         required: true
 *         schema:
 *          type: object
 *          properties:
 *            email:
 *              type: string
 *            password:
 *              type: string
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: signin
 *         schema:
 *           type: object
 *           required:
 *             - success
 *             - token
 *           properties:
 *             success:
 *               type: boolean
 *             token:
 *               type: string
 *             user:
 *               type: object
 *               properties:
 *                 id:
 *                   type: string
 *                 firstName:
 *                   type: string
 *                 lastName:
 *                   type: string
 *                 email:
 *                   type: string
 */
router.post('/signin', (req, res, next) => {
  const { email, password } = req.body;
  repo.getUserByEmail(email)
    .then((user) => {
      const userId = user.id;
      if (bcrypt.compareSync(password, user.passwordHash)) {
        const payload = { userId };
        const token = jwt.sign(payload, app.get('secret'), {
          expiresIn: 60 * 60 * 24 // expires in 24 hours
        });
        // return the information including token as JSON
        res.json({
          success: true,
          token,
          user: {
            id: user.id,
            firstName: user.firstName,
            lastName: user.lastName,
            email: user.email,
            avatarUrl: user.avatarUrl
          }
        });
      } else {
        throw new Error('Wrong password');
      }
    })
    .catch(next);
});

module.exports = router;
