const express = require('express');
const TagManager = require('./../repositories/tag.js');
const driver = require('./../utils/driver');

const router = express.Router();

const tags = new TagManager(driver);

/**
 * @swagger
 * /tags:
 *   put:
 *     description: References a tag to a node
 *     tags:
 *       - Tags
 *     parameters:
 *       - in: body
 *         name: tag
 *         type: object
 *         required: true
 *         schema:
 *           type: object
 *           properties:
 *            tag:
 *              type: string
 *            nodeId:
 *              type: string
 *         description: Tag to reference
 *     responses:
 *       200:
 *         description: Tag referenced
 *         schema:
 *           type: object
 *           required:
 *             - nodeId
 *             - tagId
 *           properties:
 *             nodeId:
 *               type: string
 *             tagId:
 *               type: string
 */
router.put('/', (req, res, next) => {
    tags.addReference(req.body.tag, req.body.nodeId)
      .then(result => res.send(result))
      .catch(next);
});

/**
 * @swagger
 * /tags:
 *   post:
 *     description: Create a new tag
 *     tags:
 *       - Tags
 *     parameters:
 *       - in: body
 *         name: tag
 *         type: string
 *         required: true
 *         description: Tag to create
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: Created relationship
 *         schema:
 *           type: object
 *           properties:
 *             id:
 *               type: string
 *             tag:
 *               type: string
 */
router.post('/', (req, res, next) => {
  tags.create(req.body.tag)
    .then((tag) => {
      res.send(tag);
    })
    .catch(next);
});

/**
 * @swagger
 * /tags/search/:keyword:
 *   get:
 *     description: Get tags with keyword in it
 *     tags:
 *       - Tags
 *     parameters:
 *       - in: path
 *         name: keyword
 *         type: string
 *         required: true
 *         description: Keyword contained in tag
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: Results sent
 *         schema:
 *           type: array
 *           items:
 *             $ref: '#/definitions/Tag'
 */
router.get('/search/:keyword', (req, res, next) => {
  tags.findByWord(req.params.keyword)
    .then(((result) => {
      res.send(result);
    }))
    .catch(next);
});

module.exports = router;
