const express = require('express');
const PostManager = require('./../repositories/post');
const DashboardManager = require('./../repositories/dashboard');
const driver = require('./../utils/driver');
const { verifyId, forbidden, getUsersId } = require('./../utils/secureByUserId');

const router = express.Router();

const posts = new PostManager(driver);
const dashboard = new DashboardManager(driver);

/**
 * @swagger
 * /posts:
 *   post:
 *     description: Create a new post
 *     tags:
 *       - Posts
 *     parameters:
 *       - in: body
 *         name: body
 *         type: object
 *         required: true
 *         schema:
 *           type: object
 *           properties:
 *            post:
 *              type: object
 *              properties:
 *                title:
 *                  type: string
 *                content:
 *                  type: string
 *            authorId:
 *              type: string
 *            replyToId:
 *              type: string
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: signin
 *         schema:
 *           $ref: '#/definitions/Post'
 */
router.post('/', (req, res, next) => {
  if (!verifyId(req, req.body.authorId)) {
    return res.status(403).json(forbidden);
  }
  posts.create(req.body.post, req.body.authorId, req.body.replyToId)
    .then(result => res.send(result))
    .catch(next);
});

/**
 * @swagger
 * /posts:
 *   delete:
 *     description: Deletes a post
 *     tags:
 *       - Posts
 *     parameters:
 *       - in: body
 *         name: id
 *         type: string
 *         required: true
 *         description: String ID of the post to delete.
 *     responses:
 *       200:
 *         description: Deleted
 */
router.delete('/', (req, res, next) => {
  const userId = getUsersId(req);
  posts.getUserPosts(userId)
    .then((userPosts) => {
      const currentPost = userPosts.find(p => p.id === req.body.id);
      if (currentPost === undefined) {
        res.status(403).json(forbidden);
      } else {
        posts.delete(req.body.id)
          .then(result => res.send(result))
          .catch(next);
      }
    })
    .catch(next);
});

/**
 * @swagger
 * /posts/:userId:
 *   get:
 *     description: Get every posts of a user
 *     tags:
 *       - Posts
 *     parameters:
 *       - in: path
 *         name: userId
 *         type: string
 *         required: true
 *         description: String ID of the user.
 *     responses:
 *       200:
 *         description: Posts
 *         schema:
 *           type: array
 *           items:
 *             $ref: '#/definitions/Post'
 */
router.get('/:userId', (req, res, next) => {
  posts.getUserPosts(req.params.userId)
    .then(result => res.send(result))
    .catch(next);
});

/**
 * @swagger
 * /posts/subscriptions/:userId:
 *   get:
 *     description: Get posts based on subscriptions of a user
 *     tags:
 *       - Posts
 *     parameters:
 *       - in: path
 *         name: userId
 *         type: string
 *         required: true
 *         description: String ID of the user.
 *     responses:
 *       200:
 *         description: Posts
 *         schema:
 *           type: array
 *           items:
 *             $ref: '#/definitions/Post'
 */
router.get('/subscriptions/:userId', (req, res, next) => {
  dashboard.getSubscribedPosts(req.params.userId)
    .then(result => res.send(result))
    .catch(next);
});

/**
 * @swagger
 * /posts/interests/:userId:
 *   get:
 *     description: Get posts based on interests of a user
 *     tags:
 *       - Posts
 *     parameters:
 *       - in: path
 *         name: userId
 *         type: string
 *         required: true
 *         description: String ID of the user.
 *     responses:
 *       200:
 *         description: Posts
 *         schema:
 *           type: array
 *           items:
 *             $ref: '#/definitions/Post'
 */
router.get('/interests/:userId', (req, res, next) => {
  dashboard.getInterestingPosts(req.params.userId)
    .then(result => res.send(result))
    .catch(next);
});

/**
 * @swagger
 * /posts/trending:
 *   get:
 *     description: Get trending posts for a user
 *     tags:
 *       - Posts
 *     responses:
 *       200:
 *         description: Posts
 *         schema:
 *           type: array
 *           items:
 *             $ref: '#/definitions/Post'
 */
router.get('/trending/', (req, res, next) => {
  dashboard.getTrendingPosts()
    .then(result => res.send(result))
    .catch(next);
});

/**
 * @swagger
 * /posts/search/:keyword:
 *   get:
 *     description: Get posts with keyword in title
 *     tags:
 *       - Posts
 *     parameters:
 *       - in: path
 *         name: keyword
 *         type: string
 *         required: true
 *         description: Keyword contained in post
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: Results sent
 *         schema:
 *           type: array
 *           items:
 *             $ref: '#/definitions/Post'
 */
router.get('/search/:keyword', (req, res, next) => {
  posts.findByWord(req.params.keyword)
    .then(((result) => {
      res.send(result);
    }))
    .catch(next);
});

module.exports = router;
