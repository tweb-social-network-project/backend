const Common = require('./common.js');

class DashboardManager {
	constructor(driver) {
		this.driver = driver;
		this.db = new Common(driver);
    }

    /**
     * Get posts of followed users
     * @param {String} userId Id of the user
     * @returns {Promise} Promise of a post collection
     */
    getSubscribedPosts(userId) {
        return new Promise((resolve, reject) => {
            // pre-conditions
            const preconditions = [
                new Promise((resolve, reject) => {
                    if (userId) {
                        const cypherGetUser = `MATCH (n:User { id: '${userId}'}) RETURN n`
                        return this.db.getSingleNode(cypherGetUser)
                            .then((user) => {
                                resolve(user)
                            })
                            .catch((error) => {
                                reject(error)
                            })
                    }
                    
                        reject(new Error('User id is missing'))
                    
                })
            ];

            // check pre-conditions
            Promise.all(preconditions)
                .then(() => {
                    const cypher = `MATCH (a:User {id: '${userId}'})-[:INTERESTED_IN]->(b:User),(b:User)-[:IS_AUTHOR_OF]->(c:Post) WITH { post: c, author: b } as post RETURN post ORDER BY post.post.created DESC`;
                    const session = this.driver.session();
                    session
                        .run(cypher)
                        .then((result) => {
                            session.close();
                            const output = result.records.map(
                                record => Object.assign(
                                    { post: record._fields[0].post.properties },
                                    { author: this.db.removeSensitiveAttributes(record._fields[0].author.properties) }
                                )
                            );
                            resolve(output);
                        })
                        .catch((error) => {
                            reject(error);
                        });
                })
                .catch((error) => {
                    reject(error);
                });
            });
    }

    /**
     * Get posts with followed tags
     * @param {String} userId Id of the user
     * @returns {Promise} Promise of a post collection
     */
    getInterestingPosts(userId) {
        return new Promise((resolve, reject) => {
            // pre-conditions
            const preconditions = [
                new Promise((resolve, reject) => {
                    if (userId) {
                        const cypherGetUser = `MATCH (n:User { id: '${userId}'}) RETURN n`
                        return this.db.getSingleNode(cypherGetUser)
                            .then((user) => {
                                resolve(user)
                            })
                            .catch((error) => {
                                reject(error)
                            })
                    }
                    
                        reject(new Error('User id is missing'))
                    
                })
            ];

            // check pre-conditions
            Promise.all(preconditions)
                .then(() => {
                    const cypher = `MATCH (a:User {id: '${userId}'})-[:INTERESTED_IN]->(b:Tag),(c:User)-[:IS_AUTHOR_OF]->(d:Post),(d:Post)-[:REFERENCES]->(b:Tag) OPTIONAL MATCH (e)-[r:LIKES]->(d:Post) WITH { post: d, author: c, tag:b, likes: COUNT(r) } as post RETURN post ORDER BY post.likes DESC`;
                    const session = this.driver.session();
                    session
                        .run(cypher)
                        .then((result) => {
                            session.close();
                            const output = result.records.map(
                                record => Object.assign(
                                    { post: record._fields[0].post.properties },
                                    { author: this.db.removeSensitiveAttributes(record._fields[0].author.properties) },
                                    { tag: record._fields[0].tag.properties },
                                    { likes: record._fields[0].likes }
                                )
                            );
                            resolve(output);
                        })
                        .catch((error) => {
                            reject(error);
                        });
                })
                .catch((error) => {
                    reject(error);
                });
            });
    }

    /**
     * Get trending posts
     * @returns {Promise} Promise of a post collection
     */
    getTrendingPosts() {
        return new Promise((resolve, reject) => {
            const cypher = 'MATCH (c:User)-[:IS_AUTHOR_OF]->(d:Post) OPTIONAL MATCH (e)-[r:LIKES]->(d:Post) OPTIONAL MATCH (a:Post)-[s:REPLIES_TO]->(d:Post) WITH { post: d, author: c, likes: COUNT(r), answers: COUNT(s) } as post RETURN post ORDER BY post.answers DESC, post.likes DESC';
            const session = this.driver.session();
            session
                .run(cypher)
                .then((result) => {
                    session.close();
                    const output = result.records.map(
                        record => Object.assign(
                            { post: record._fields[0].post.properties },
                            { author: this.db.removeSensitiveAttributes(record._fields[0].author.properties) },
                            { answers: record._fields[0].answers },
                            { likes: record._fields[0].likes }
                        )
                    );
                    resolve(output);
                })
                .catch((error) => {
                    reject(error);
                });
        });
    }
}

module.exports = DashboardManager;
