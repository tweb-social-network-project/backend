

const Common = require('./common.js');

class TagManager {
	constructor(driver) {
		this.driver = driver;
		this.db = new Common(driver);
    }

    /**
     * Add a like to a node
     * @param {String} userId Id of the user
     * @param {String} nodeId Id of the object to like
     * @returns {Promise} Promise of a like relationship
     */
    addLike(userId, nodeId) {
        return new Promise((resolve, reject) => {
            // pre-conditions
            const preconditions = [
                new Promise((resolve, reject) => {
                    if (userId) {
                        const cypherGetUser = `MATCH (n:User { id: '${userId}'}) RETURN n`;
                        return this.db.getSingleNode(cypherGetUser)
                            .then((user) => {
                                resolve(user);
                            })
                            .catch((error) => {
                                reject(error);
                            });
                    }

                    reject(new Error('User id is missing'));
                }),
                new Promise((resolve, reject) => {
                    if (nodeId) {
                        const cypherGetNode = `MATCH (n { id: '${nodeId}'}) RETURN n`;
                        return this.db.getSingleNode(cypherGetNode)
                            .then((node) => {
                                resolve(node);
                            })
                            .catch((error) => {
                                reject(error);
                            });
                    }

                    reject(new Error('Node id is missing'));
                })
            ];

            // check pre-conditions
            Promise.all(preconditions)
                .then(() => {
                    // check if relationship already exist
                    const cypherCheck = `MATCH (a:User { id: '${userId}'})-[r:LIKES]->(b { id: '${nodeId}'}) RETURN r`;
                    this.db.getMultipleNodes(cypherCheck)
                        .then((relationships) => {
                            if (relationships.length !== 0) {
                                throw new Error(`Source '${userId}' has already liked node '${nodeId}'`);
                            }

                            const cypherAddLike = `MATCH (a:User { id: '${userId}'}),(b { id: '${nodeId}'}) CREATE (a)-[r:LIKES]->(b) RETURN r`;
                            this.db.getSingleNode(cypherAddLike)
                                .then((like) => {
                                    resolve(like);
                                })
                                .catch((error) => {
                                    reject(error);
                                });
                        })
                        .catch((error) => {
                            reject(error);
                        });
                })
                .catch((error) => {
                    reject(error);
                });
            });
    }

    /**
     * Get liked nodes
     * @param {String} userId Id of the user
     * @returns {Promise} Promise of a node collection
     */
    getLikedItems(userId) {
        return new Promise((resolve, reject) => {
            // pre-conditions
            const preconditions = [
                new Promise((resolve, reject) => {
                    if (userId) {
                        const cypherGetNode = `MATCH (n { id: '${userId}'}) RETURN n`;
                        return this.db.getSingleNode(cypherGetNode)
                            .then((node) => {
                                resolve(node);
                            })
                            .catch((error) => {
                                reject(error);
                            });
                    }

                        reject(new Error('User id is missing'));

                })
            ];

            // check pre-conditions
            Promise.all(preconditions)
                .then(() => {
                    const cypherGetLikes = `MATCH (a:User { id: '${userId}'})-[r:LIKES]->(b) RETURN b`;
                    this.db.getMultipleNodesWithType(cypherGetLikes)
                        .then((like) => {
                            resolve(like);
                        })
                        .catch((error) => {
                            reject(error);
                        });
                })
                .catch((error) => {
                    reject(error);
                });
            });
    }

    /**
     * Get likers of a node
     * @param {String} nodeId Id of the node (e.g. post)
     * @returns {Promise} Promise of a node collection
     */
    getLikers(nodeId) {
        return new Promise((resolve, reject) => {
            // pre-conditions
            const preconditions = [
                new Promise((resolve, reject) => {
                    if (nodeId) {
                        const cypherGetNode = `MATCH (n { id: '${nodeId}'}) RETURN n`;
                        return this.db.getSingleNode(cypherGetNode)
                            .then((node) => {
                                resolve(node);
                            })
                            .catch((error) => {
                                reject(error);
                            });
                    }

                        reject(new Error('Node id is missing'));

                })
            ];

            // check pre-conditions
            Promise.all(preconditions)
                .then(() => {
                    const cypherGetLikes = `MATCH (a:User)-[r:LIKES]->(b { id: '${nodeId}'}) RETURN a`;
                    this.db.getMultipleNodes(cypherGetLikes)
                        .then((like) => {
                            resolve(like);
                        })
                        .catch((error) => {
                            reject(error);
                        });
                })
                .catch((error) => {
                    reject(error);
                });
            });
    }
}

module.exports = TagManager;
