const uuidv4 = require('uuid/v4');
const { inspect } = require('util');
const Common = require('./common.js');

/**
 * Manage interest relationships
 */
class InterestManager {
    constructor(driver) {
        this.driver = driver;
        this.db = new Common(driver);
    }

    /**
     * Add an "INTERESTED_IN" relationship between two nodes
     * @param {string} sourceId the id of the node (e.g. user) interested in something
     * @param {string} targetId the id of the interest (e.g. a tag)
     * @param {Object} properties optional relationship properties (e.g. level of interest)
     * @returns {Promise} Promise of the created relationship
     */
    addInterest(sourceId, targetId, properties) {
        return new Promise((resolve, reject) => {
            // check required fields
            if (!sourceId) {
                throw new Error('Source id is missing');
            }
            if (!targetId) {
                throw new Error('Target id is missing');
            }

            // check if relationship already exist
            const cypherCheck = `MATCH (a { id: '${sourceId}'})-[r:INTERESTED_IN]->(b { id: '${targetId}'}) RETURN r`;
            this.db.getMultipleNodes(cypherCheck)
                .then((relationships) => {
                    if (relationships.length !== 0) {
                        throw new Error(`Source '${sourceId}' is already interested in target '${targetId}'`);
                    }

                    // add properties
                    let relationshipProperties = {};
                    if (properties) {
                        relationshipProperties = properties;
                    }

                    // set id
                    relationshipProperties.id = uuidv4();

                    const cypher = `MATCH (a { id: '${sourceId}'}),(b { id: '${targetId}'}) CREATE (a)-[r:INTERESTED_IN ${inspect(relationshipProperties)}]->(b) RETURN r`;
                    this.db.getSingleNode(cypher)
                        .then((createdRelationship) => {
                            resolve(createdRelationship);
                        })
                        .catch((error) => {
                            reject(error);
                        });
                })
                .catch((error) => {
                    reject(error);
                });
        });
    }

    /**
     * Update an existing "INTERESTED_IN" relationship
     * @param {relationship} relationship the relationship to update
     * @returns {Promise} Promise of the updated relationship
     */
    updateInterest(relationship) {
        return new Promise((resolve, reject) => {
            // check required fields
            if ('id' in relationship === false) {
                reject(new Error('Relationship id is missing'));
            }

            const cypher = `MATCH ()-[r:INTERESTED_IN { id: '${relationship.id}'}]-() SET r = ${inspect(relationship)} RETURN r LIMIT 1`;
            this.db.getSingleNode(cypher)
                .then((updatedRelationship) => {
                    resolve(updatedRelationship);
                })
                .catch((error) => {
                    reject(error);
                });
          });
    }

    /**
     * Remove a "INTERESTED_IN" relationship
     * @param {relationship} relationship the relationship to remove
     * @returns {Promise} Promise of the delete operation
     */
    removeInterest(relationship) {
        return new Promise((resolve, reject) => {
            // check required fields
            if ('id' in relationship === false) {
                reject(new Error('Relationship id is missing'));
            }

            const cypher = `MATCH ()-[r:INTERESTED_IN { id: '${relationship.id}'}]-() DELETE r`;
            this.db.doOperationWithoutResult(cypher)
                .then(() => {
                    resolve();
                })
                .catch((error) => {
                    reject(error);
                });
          });
    }

    /**
     * Get outgoing "INTERESTED_IN" relationships of a node
     * @param {string} sourceId the source node id
     * @returns {Promise} Promise of the outgoing relationships
     */
    getInterests(sourceId) {
        return new Promise((resolve, reject) => {
            // check required fields
            if (!sourceId) {
                reject(new Error('Source id is missing'));
            }

            const cypher = `MATCH (a { id: '${sourceId}' })-[r:INTERESTED_IN]->(b) RETURN b`;
            this.db.getMultipleNodesWithType(cypher)
                .then((like) => {
                    resolve(like);
                })
                .catch((error) => {
                    reject(error);
                });
          });
    }

    /**
     * Count outgoing "INTERESTED_IN" relationships of a node
     * @param {string} sourceId the source node id
     * @returns {Promise} Promise of the number of relationships
     */
    getInterestCount(sourceId) {
        return new Promise((resolve, reject) => {
            // check required fields
            if (!sourceId) {
                reject(new Error('Source id is missing'));
            }

            const cypher = `MATCH (a { id: '${sourceId}' })-[r:INTERESTED_IN]->(b) RETURN count(r)`;
            const session = this.driver.session();
            session
                .run(cypher)
                .then((result) => {
                    session.close();
                    const output = result.records[0]._fields[0]
                    resolve(output);
                })
                .catch((error) => {
                    reject(error);
                });
        });
    }

    /**
     * Get incoming "INTERESTED_IN" relationships of a node
     * @param {string} targetId the target node
     * @returns {Promise} Promise of the incoming relationships
     */
    getFollowers(targetId) {
        return new Promise((resolve, reject) => {
            // check required fields
            if (!targetId) {
                reject(new Error('Target id is missing'));
            }

            const cypher = `MATCH (a)-[r:INTERESTED_IN]->(b { id: '${targetId}' }) RETURN a`;
            this.db.getMultipleNodesWithType(cypher)
                .then((followers) => {
                    resolve(followers);
                })
                .catch((error) => {
                    reject(error);
                });
          });
    }

    /**
     * Count incoming "INTERESTED_IN" relationships of a node
     * @param {string} targetId the target node
     * @returns {Promise} Promise of the number of relationships
     */
    getFollowerCount(targetId) {
        return new Promise((resolve, reject) => {
            // check required fields
            if (!targetId) {
                reject(new Error('Target id is missing'));
            }

            const cypher = `MATCH (a)-[r:INTERESTED_IN]->(b { id: '${targetId}' }) RETURN count(r)`;
            const session = this.driver.session();
            session
                .run(cypher)
                .then((result) => {
                    session.close();
                    const output = result.records[0]._fields[0]
                    resolve(output);
                })
                .catch((error) => {
                    reject(error);
                });
        });
    }
}

module.exports = InterestManager;
