

const uuidv4 = require('uuid/v4');
const Common = require('./common.js');
const TagManager = require('./tag.js');
const { inspect } = require('util');

/**
 * Extract tags from a text
 * @param {String} text Text to parse for tags
 * @returns {String[]} An array of tags
 */
function extractTags(text) {
    const regexp = /(\s|^)\#\w\w+\b/gm;
    const results = text.match(regexp);
    if (results) {
        return results.map(tag => tag.trim());
    }
        return [];
}

class PostManager {
	constructor(driver) {
        this.driver = driver;
        this.db = new Common(driver);
	}

	/**
	 * Create a post
	 * @param {Object} post Object describing the post
	 * @param {string} authorId User id of the author
	 * @param {string} replyToId Post id of which this post is an answer
	 * @returns {Promise} Promise of the created post
	 */
	create(post, authorId, replyToId) {
		return new Promise((resolve, reject) => {
            // pre-conditions
            const preconditions = [
                new Promise((resolve, reject) => {
                    if ('content' in post === true) {
                        resolve('Post content ok');
                    } else {
                        reject(new Error('Content is missing'));
                    }
                }),
                new Promise((resolve, reject) => {
                    if (authorId) {
                        const cypherGetAuthor = `MATCH (n:User { id: '${authorId}'}) RETURN n`;
                        return this.db.getSingleNode(cypherGetAuthor)
                            .then((user) => {
                                resolve(user);
                            })
                            .catch((error) => {
                                reject(error);
                            });
                    }

                        reject(new Error('Author id is missing'));
                }),
                new Promise((resolve, reject) => {
                    if (replyToId) {
                        const cypherGetPost = `MATCH (n:Post { id: '${replyToId}'}) RETURN n`;
                        return this.db.getSingleNode(cypherGetPost)
                            .then(() => {
                                resolve('ReplyTo post ok');
                            })
                            .catch((error) => {
                                reject(error);
                            });
                    }

                        resolve('ReplyTo post not specified');
                })
            ];

            // check pre-conditions
            Promise.all(preconditions)
                .then(() => {
                    // set id
                    post.id = uuidv4();

                    // set created
                    post.created = Date.now();

                    // create post
                    const cypherCreatePost = `CREATE (n:Post ${inspect(post)}) RETURN n`;
                    this.db.getSingleNode(cypherCreatePost)
                        .then((createdPost) => {
                            // link post to author
                            const cypherAddAuthor = `MATCH (a:User { id: '${authorId}'}),(b:Post { id: '${post.id}'}) CREATE (a)-[r:IS_AUTHOR_OF]->(b) RETURN r`;
                            this.db.getSingleNode(cypherAddAuthor)
                                .then(() => {
                                    // handle tags in content
                                    const tagManager = new TagManager(this.driver);
                                    const tags = extractTags(post.content);
                                    const promises = tags.map(tag => tagManager.checkIfExist(tag)
                                            .then((exist) => {
                                                if (!exist) {
                                                    return tagManager.create(tag)
                                                        .then(() => tagManager.addReference(tag, createdPost.id));
                                                }

                                                    return tagManager.addReference(tag, createdPost.id);
                                            }));

                                    return Promise.all(promises);
                                })
                                .then(() => {
                                    // link answer to post
                                    if (replyToId) {
                                        const cypherLinkAnswer = `MATCH (a:Post {id: '${createdPost.id}'}),(b:Post {id: '${replyToId}'}) CREATE (a)-[r:REPLIES_TO]->(b) RETURN r`;
                                        return this.db.getSingleNode(cypherLinkAnswer);
                                    }

                                        return Promise.resolve();
                                })
                                .then(() => {
                                    resolve(createdPost);
                                })
                                .catch((error) => {
                                    reject(error);
                                });
                        })
                        .catch((error) => {
                            reject(error);
                        });
                })
                .catch((error) => {
                    reject(error);
                });
		});
    }

    /**
	 * Get all posts
	 * @returns {Promise} Promise of a post collection
     */
    getAll() {
        return new Promise((resolve, reject) => {
            const cypher = 'MATCH (a:User)-[r:IS_AUTHOR_OF]->(b:Post) WITH { post: b, author: a } as post RETURN post';
            const session = this.driver.session();
            session
                .run(cypher)
                .then((result) => {
                    session.close();
                    const output = result.records.map(
                        record => Object.assign(
                            { post: record._fields[0].post.properties },
                            { author: this.db.removeSensitiveAttributes(record._fields[0].author.properties) }
                        )
                    );
                    resolve(output);
                })
                .catch((error) => {
                    reject(error);
                });
        });
    }

    /**
	 * Get all posts from a specific user
     * @param {string} userId The user's id
	 * @returns {Promise} Promise of a post collection
     */
    getUserPosts(userId) {
        return new Promise((resolve, reject) => {
            const cypher = `MATCH (u:User { id: '${userId}' })-[r:IS_AUTHOR_OF]->(p:Post) RETURN p ORDER BY p.created DESC`;
            this.db.getMultipleNodes(cypher)
                .then((posts) => {
                    resolve(posts);
                })
                .catch((error) => {
                    reject(error);
                });
        });
    }

    /**
	 * Get all answers to a specific post
     * @param {string} postId The post id
	 * @returns {Promise} Promise of a post collection
     */
    getAnswers(postId) {
        return new Promise((resolve, reject) => {
            const cypher = `MATCH (u:User)-[:IS_AUTHOR_OF]->(a:Post),(a:Post)-[r:REPLIES_TO]->(b:Post { id: '${postId}' }) WITH { answer: a, author: u } as answer RETURN answer`;
            const session = this.driver.session();
            session
                .run(cypher)
                .then((result) => {
                    session.close();
                    const output = result.records.map(
                        record => Object.assign(
                            { answer: record._fields[0].answer.properties },
                            { author: this.db.removeSensitiveAttributes(record._fields[0].author.properties) }
                        )
                    );
                    resolve(output);
                })
                .catch((error) => {
                    reject(error);
                });
        });
    }

    /**
     * Delete a post and all its relationships
     * @param {String} postId The post id
     * @returns {Promise} Promise of the delete operation
     */
    delete(postId) {
        return new Promise((resolve, reject) => {
            // pre-conditions
            const preconditions = [
                new Promise((resolve, reject) => {
                    if (postId) {
                        const cypherGetPost = `MATCH (n:Post { id: '${postId}'}) RETURN n`;
                        return this.db.getSingleNode(cypherGetPost)
                            .then((post) => {
                                resolve(post);
                            })
                            .catch((error) => {
                                reject(error);
                            });
                    }

                        reject('Post id is missing');
                })
            ];

            // check pre-conditions
            Promise.all(preconditions)
                .then(() => {
                    const cypherDeletePost = `MATCH (n:Post { id: '${postId}'}) DETACH DELETE n`;
                    this.db.doOperationWithoutResult(cypherDeletePost)
                        .then(() => {
                            resolve();
                        })
                        .catch((error) => {
                            reject(error);
                        });
                })
                .catch((error) => {
                    reject(error);
                });
        });
    }    

    /**
	 * Find posts
     * @param {String} word The word to search in post content
	 * @returns {Promise} Promise of a post collection
     */
    findByWord(word) {
        return new Promise((resolve, reject) => {
            // check required fields
			if (!word) {
				reject(new Error('Word is empty'))
			}
            const cypher = `MATCH (u:User)-[:IS_AUTHOR_OF]->(p:Post) WHERE p.content =~ '(?i).*${word}.*' WITH { post: p, author: u } as post RETURN post`;
            const session = this.driver.session();
            session
                .run(cypher)
                .then((result) => {
                    session.close();
                    const output = result.records.map(
                        record => Object.assign(
                            { post: record._fields[0].post.properties },
                            { author: this.db.removeSensitiveAttributes(record._fields[0].author.properties) }
                        )
                    );
                    resolve(output);
                })
                .catch((error) => {
                    reject(error);
                });
        });
    }
}

module.exports = PostManager;
