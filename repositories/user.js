const uuidv4 = require('uuid/v4');
const { inspect } = require('util');
const Common = require('./common.js');

/**
 * Manage users
 */
class UserRepository {
	constructor(driver) {
		this.driver = driver;
        this.db = new Common(driver);
	}

	/**
	 * Create a user
	 * @param {Object} user User information
	 * @returns {Promise} Promise of the created user
	 */
	create(user) {
		return new Promise((resolve, reject) => {
			// check required fields
			if ('email' in user === false) {
				reject(new Error('Email is missing'));
			}
			if ('passwordHash' in user === false) {
				reject(new Error('Password is missing'));
			}

			// set id
			user.id = uuidv4();

			const cypher = `CREATE (n:User ${inspect(user)}) RETURN n`;
			this.db.getSingleNode(cypher)
				.then((createdUser) => {
					resolve(createdUser);
				})
				.catch((error) => {
					reject(error);
				});
		});
	}

	/**
	 * Validate user credentials
	 * @param {string} email the user's email address
	 * @param {string} passwordHash the user's password hash
	 * @returns {Promise} Promise of the user
	 */
	getByCredentials(email, passwordHash) {
		return new Promise((resolve, reject) => {
			const cypher = `MATCH (n:User) WHERE n.email =~ '(?i)${email}' AND n.passwordHash = '${passwordHash}' RETURN n`;
			this.db.getSingleNode(cypher)
				.then((user) => {
					resolve(user);
				})
				.catch((error) => {
					reject(error);
				});
		});
	}

	/**
	 * Get user by id
	 * @param {string} userId the user's id
	 * @returns {Promise} Promise of the user
	 */
	getById(userId) {
		return new Promise((resolve, reject) => {
			// check required fields
            if (!userId) {
                reject(new Error('User id is missing'));
			}

			const cypher = `MATCH (n:User {id: '${userId}'}) RETURN n`;
			this.db.getSingleNode(cypher)
				.then((user) => {
					resolve(user);
				})
				.catch((error) => {
					reject(error);
				});
			});
	}

	/**
	 * Get a user's password hash
	 * @param {string} email the user's email address
	 * @returns {Promise} Promise of the password hash
	 */
	getPasswordHashByEmail(email) {
		return new Promise((resolve, reject) => {
			const cypher = `MATCH (n:User) WHERE n.email =~ '(?i)${email}' RETURN n.passwordHash`;
			const session = this.driver.session();
			session
				.run(cypher)
				.then((result) => {
					session.close();
					if (result.records.length > 0) {
						const singleRecord = result.records[0];
						const passwordHash = singleRecord.get(0);
						resolve(passwordHash);
					} else {
						reject(new Error('No user found'));
					}
				})
				.catch((error) => {
					reject(error);
				});
		});
	}

	/**
	 * Get a user by email
	 * @param {string} email the user's email address
	 * @returns {Promise} Promise of the user
	 */
	getUserByEmail(email) {
		return new Promise((resolve, reject) => {
			const cypher = `MATCH (n:User) WHERE n.email =~ '(?i)${email}' RETURN n`;
			this.db.getSingleNode(cypher)
				.then((user) => {
					resolve(user);
				})
				.catch((error) => {
					reject(error);
				});
		});
	}

	getUserById(id) {
		return new Promise((resolve, reject) => {
			const cypher = `MATCH (n:User {id: '${id}'}) RETURN n`;
			this.db.getSingleNode(cypher)
				.then((user) => {
					resolve(user);
				})
				.catch((error) => {
					reject(error);
				});
		});
	}

	/**
	 * Update a user
	 * @param {Object} user User information
	 * @returns {Promise} Promise of the updated user
	 */
	update(user) {
		return new Promise((resolve, reject) => {
			// check if user exist
			const cypherGet = `MATCH (n:User {id: '${user.id}'}) RETURN n`;
			this.db.getSingleNode(cypherGet)
				.then((currentUser) => {
					// preserve old password
					if ('passwordHash' in user === false) {
						user.passwordHash = currentUser.passwordHash;
					}
					const cypherUpdate = `MATCH (n:User {id: '${user.id}'}) SET n = ${inspect(user)} RETURN n`;
					this.db.getSingleNode(cypherUpdate)
						.then((updatedUser) => {
							resolve(updatedUser);
						})
						.catch((error) => {
							reject(error);
						});
					})
				.catch((error) => {
					reject(error);
				});
		});
	}

	/**
	 * Delete a user
	 * @param {string} userId The user id
	 * @returns {Promise} Promise of the delete operation
	 */
	delete(userId) {
		return new Promise((resolve, reject) => {
			const cypher = `MATCH (n:User {id: '${userId}'}) DETACH DELETE n`;
			this.db.doOperationWithoutResult(cypher)
                .then(() => {
                    resolve();
                })
                .catch((error) => {
                    reject(error);
                });
		});
	}

	/**
	 * Get all users
	 * @returns {Promise} Promise of a user array
	 */
	getAll() {
		return new Promise((resolve, reject) => {
			const cypher = 'MATCH (n:User) RETURN n';
			const session = this.driver.session();
            session
                .run(cypher)
                .then((result) => {
                    session.close();
                    const users = result.records.map(
                        record => this.db.removeSensitiveAttributes(
							Object.assign(record._fields[0].properties)
                        )
                    );
                    resolve(users);
                })
                .catch((error) => {
                    reject(error);
                });
		});
	}

	/**
	 * Check if an email address is already used
	 * @param {string} email an email address
	 * @returns {Promise} Promise of a boolean
	 * (true if the email address is already used, false otherwise)
	 */
	isEmailUsed(email) {
		return new Promise((resolve, reject) => {
			const cypher = `MATCH (n:User) WHERE n.email =~ '(?i)${email}' RETURN n`;
			const session = this.driver.session();
			session
				.run(cypher)
				.then((result) => {
					session.close();
					resolve(result.records.length > 0);
				})
				.catch((error) => {
					reject(error);
				});
		});
	}

    /**
	 * Find users
     * @param {String} word The word to search
	 * @returns {Promise} Promise of a user collection
     */
    findByWord(word) {
        return new Promise((resolve, reject) => {
            // check required fields
			if (!word) {
				throw new Error('Word is empty');
			}
            const cypher = `MATCH (u:User) WHERE split(u.email, "@")[0] =~ '(?i).*${word}.*' OR u.firstName =~ '(?i).*${word}.*' OR u.lastName =~ '(?i).*${word}.*' RETURN u`;
            const session = this.driver.session();
            session
                .run(cypher)
                .then((result) => {
                    session.close();
                    const output = result.records.map(
                        record => this.db.removeSensitiveAttributes(
							Object.assign(record._fields[0].properties)
                        )
                    );
                    resolve(output);
                })
                .catch((error) => {
                    reject(error);
                });
        });
    }
}

module.exports = UserRepository;
