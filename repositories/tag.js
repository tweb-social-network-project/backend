'use strict'

const uuidv4 = require('uuid/v4');
const Common = require('./common.js');

class TagManager {
	constructor(driver) {
		this.driver = driver;
		this.db = new Common(driver);
	}

	/**
	 * Check if a tag exist
	 * @param {string} tag The tag
	 * @returns {Promise} Promise of a boolean (true if the tag exist, false otherwise)
	 */
	checkIfExist(tag) {
		return new Promise((resolve, reject) => {
			// check required fields
			if (!tag) {
				reject(new Error('Tag is empty'));
			}

			const cypher = `MATCH (n:Tag {name: '${tag}'}) RETURN n`;
			const session = this.driver.session();
			session
				.run(cypher)
				.then((tags) => {
					resolve(tags.records.length !== 0);
				})
				.catch((error) => {
					reject(error);
				});
		});
	}

	/**
	 * Create a tag
	 * @param {string} tag
	 * @returns {Promise} Promise of the created tag
	 */
	create(tag) {
		return new Promise((resolve, reject) => {
			// check required fields
			if (!tag) {
				reject(new Error('Tag is empty'));
			}
			if (!tag.startsWith('#')) {
				reject(new Error('Tag does not start with hashtag'));
			}

			// check if tag already exist
			this.checkIfExist(tag)
				.then((exist) => {
					if (!exist) {
						const cypher = `CREATE (n:Tag {id: '${uuidv4()}', name: '${tag}'}) RETURN n`;
						this.db.getSingleNode(cypher)
							.then((tag) => {
								resolve(tag);
							})
							.catch((error) => {
								reject(error);
							});
					} else {
						reject(new Error('Tag already exist'));
					}
				})
				.catch((error) => {
					reject(error);
				});
		});
	}

	/**
	 * Get tag by name
	 * @param {string} tag The tag
	 * @returns {Promise} Promise of a tag
	 */
	getTagByName(tag) {
		return new Promise((resolve, reject) => {
			// check required fields
			if (!tag) {
				reject(new Error('Tag is empty'));
			}

			const cypher = `MATCH (n:Tag {name: '${tag}'}) RETURN n`;
			this.db.getSingleNode(cypher)
				.then((tag) => {
					resolve(tag);
				})
				.catch((error) => {
					reject(error);
				});
		});
	}

	/**
	 * Add reference from an existing node to an existing tag
	 * @param {string} tag The tag
	 * @param {string} nodeId The id of the node that references the tag
	 * @returns {Promise} Promise of the created relationship
	 */
	addReference(tag, nodeId) {
		return new Promise((resolve, reject) => {
			// check required fields
			if (!tag) {
				reject(new Error('Tag is empty'));
			}
			if (!nodeId) {
				reject(new Error('Node id is empty'));
			}

			const cypher = `MATCH (a {id : '${nodeId}'}),(b:Tag {name: '${tag}'}) CREATE (a)-[r:REFERENCES]->(b) RETURN r`;
			this.db.getSingleNode(cypher)
				.then((relationship) => {
					resolve(relationship);
				})
				.catch((error) => {
					reject(error);
				});
		});
	}

	/**
	 * Get all nodes that reference a specific tag
	 * @param {string} tag The referenced tag
	 * @returns {Promise} Promise of all referencing nodes
	 */
	getReferencingNodes(tag) {
		return new Promise((resolve, reject) => {
			// check required fields
			if (!tag) {
				reject(new Error('Tag is empty'));
			}

			const cypher = `MATCH (a)-[r:REFERENCES]->(b:Tag {name: '${tag}'}) RETURN a`;
			this.db.getMultipleNodesWithType(cypher)
				.then((like) => {
					resolve(like);
				})
				.catch((error) => {
					reject(error);
				});
		});
	}

	/**
	 * Get all tags
	 * @returns Promise of a tag array
	 */
	getAll() {
		return new Promise((resolve, reject) => {
			const cypher = 'MATCH (n:Tag) RETURN n';
			this.db.getMultipleNodes(cypher)
				.then((tags) => {
					resolve(tags);
				})
				.catch((error) => {
					reject(error);
				});
		});
	}

	/**
	 * Find tags
	 * @param {String} word A word to search in existing tags
	 * @returns Promise of a tag array
	 */
	findByWord(word) {
		return new Promise((resolve, reject) => {
			// check required fields
			if (!word) {
				reject(new Error('Word is empty'));
			}
			const cypher = `MATCH (n:Tag) WHERE n.name =~ '(?i).*${word}.*' RETURN n`;
			this.db.getMultipleNodes(cypher)
				.then((tags) => {
					resolve(tags);
				})
				.catch((error) => {
					reject(error);
				});
		});
	}
}

module.exports = TagManager;
