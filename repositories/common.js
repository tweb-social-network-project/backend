'use strict';

/**
 * Common methods for neo4j database access
 */
class Common {
    constructor(driver) {
        this.driver = driver;
    }

    /**
     * Remove sensitive properties on a node (e.g. password hash)
     * @param {node} node Node to clean
     * @returns {node} The sanitized node
     */
    removeSensitiveAttributes(node) {
        if ('passwordHash' in node === true) {
            delete node.passwordHash;
        }
        return node;
    }

    /**
     * Returns no nodes
     * @param {string} cypher Cypher query
     * @returns {Promise} Promise of the operation result
     */
    doOperationWithoutResult(cypher) {
        return new Promise((resolve, reject) => {
            const session = this.driver.session();
            session
                .run(cypher)
                .then((result) => {
                    session.close();
                    if (result.records.length === 0) {
                        resolve();
                    }
                    else {
                        reject(new Error('No result expected, but cypher query returned ' + result.records.length + ' records'));
                    }
                })
                .catch((error) => {
                    reject(error);
                });
        });
    }

    /**
     * Returns a single node
     * @param {string} cypher Cypher query
     * @returns {Promise} Promise of result node
     */
    getSingleNode(cypher) {
        return new Promise((resolve, reject) => {
            const session = this.driver.session();
            session
                .run(cypher)
                .then((result) => {
                    session.close();
                    if (result.records.length === 1) {
                        const output = result.records[0]._fields[0].properties;
                        resolve(output);
                    }
                    else {
                        reject(new Error('Single node expected, but cypher query returned ' + result.records.length + ' records'));
                    }
                })
                .catch((error) => {
                    reject(error);
                });
        });
    }

    /**
     * Returns a collection of nodes
     * @param {string} cypher Cypher query
     * @returns {Promise} Promise of a collection of nodes
     */
    getMultipleNodes(cypher) {
        return new Promise((resolve, reject) => {
            const session = this.driver.session();
            session
                .run(cypher)
                .then((result) => {
                    session.close();
                    const output = result.records.map(
                        record => this.removeSensitiveAttributes(Object.assign(record._fields[0].properties))
                    );
                    resolve(output);
                })
                .catch((error) => {
                    reject(error);
                });
        });
    }

    /**
     * Returns a collection of nodes with their type
     * @param {string} cypher Cypher query
     * @returns {Promise} Promise of a collection of nodes
     */
    getMultipleNodesWithType(cypher) {
        return new Promise((resolve, reject) => {
            const session = this.driver.session();
            session
                .run(cypher)
                .then((result) => {
                    session.close();
                    const output = result.records.map(
                        record => this.removeSensitiveAttributes(
                            Object.assign(
                                record._fields[0].properties,
                                { type: record._fields[0].labels[0] } // we don't expect to use multiple labels per node
                            )
                        )
                    );
                    resolve(output);
                })
                .catch((error) => {
                    reject(error);
                });
        });
    }
}

module.exports = Common;