const express = require('express');
const jwt = require('jsonwebtoken');
const config = require('../config.js');

const app = express();

app.set('secret', config.secret);


const forbidden = {
  success: false,
  message: 'Forbidden'
};

function verifyId(req, id) {
  const token = req.body.token || req.query.token || req.headers['x-access-token'];
  let idOk = true;
  jwt.verify(token, app.get('secret'), (err, decoded) => {
    req.decoded = decoded;
    const userId = decoded.userId;
    if (id !== userId) {
      idOk = false;
    }
  });
  return idOk;
}

function getUsersId(req) {
  let userId;
  const token = req.body.token || req.query.token || req.headers['x-access-token'];
  jwt.verify(token, app.get('secret'), (err, decoded) => {
    req.decoded = decoded;
    userId = decoded.userId;
  });
  return userId;
}

module.exports.forbidden = forbidden;
module.exports.verifyId = verifyId;
module.exports.getUsersId = getUsersId;
