const neo4j = require('neo4j-driver').v1;

require('dotenv').config();


const driver = neo4j.driver(
    process.env.GRAPHENEDB_BOLT_URL,
    neo4j.auth.basic(process.env.GRAPHENEDB_BOLT_USER, process.env.GRAPHENEDB_BOLT_PASSWORD),
  { disableLosslessIntegers: true }
);

module.exports = driver;
